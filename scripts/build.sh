#!/usr/bin/env sh

set -e

cmake -B build -G Ninja -DCMAKE_BUILD_TYPE=Release
cmake --build build
