#!/usr/bin/env bash

# Add the filter using git config filter.remove-secrets.clean scripts/remove-secrets.sh
# Then add the following line to the .gitattributes file to automatically apply the filter
# src/secrets.hpp filter=remove-secrets

sed -e 's/^#define\s\(\([[:alnum:]]\|_\)*\s\).*/#define \1\"\"/' "$@"
