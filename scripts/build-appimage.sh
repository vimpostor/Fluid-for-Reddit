#!/usr/bin/env bash

set -e

LINUXDEPLOYQT_URL='https://github.com/probonopd/linuxdeployqt/releases/download/7/linuxdeployqt-7-x86_64.AppImage'
DEPLOY='./linuxdeployqt.AppImage'
BUILD='build-linuxdeployqt'
ROOT_PATH="$BUILD/prefix"
PREFIX="$ROOT_PATH/appdir/usr"

mkdir -p "$PREFIX"
echo 'Building the project...'
cmake -B "$BUILD" -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$PREFIX"
cmake --build "$BUILD"
cmake --build "$BUILD" --target linux-desktop-integration
cmake --install "$BUILD"

pushd "$ROOT_PATH"
echo 'Running linuxdeployqt...'
curl -sL "$LINUXDEPLOYQT_URL" -o "$DEPLOY"
chmod +x "$DEPLOY"
# Workaround for weird check from linuxdeployqt
mkdir -p "$PREFIX/share/doc/libc6"
touch "$PREFIX/share/doc/libc6/copyright"
"./$DEPLOY" "appdir/usr/share/applications/fluid-for-reddit.desktop" -unsupported-bundle-everything -appimage
popd
