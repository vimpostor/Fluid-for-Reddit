#include "downloader.hpp"

Downloader::Downloader(QObject* parent) : QObject(parent)
{
}

void Downloader::queueDownload(const std::shared_ptr<MediaSource>& mediaSource)
{
	downloadQueue.push(mediaSource);
	tryNextDownload();
}

void Downloader::download(const std::shared_ptr<MediaSource>& mediaSource)
{
	qDebug() << "Downloading " << mediaSource->get_source();
	QNetworkRequest networkRequest(mediaSource->get_source());
	auto* reply = Util::getNetworkAccessManager()->get(networkRequest);
	connect(reply, &QNetworkReply::finished, [=, this](){ downloadReady(reply, mediaSource); });
	connect(reply, &QNetworkReply::downloadProgress, mediaSource.get(), &MediaSource::processDownloadProgress);
}

void Downloader::downloadReady(QNetworkReply* reply, const std::shared_ptr<MediaSource>& mediaSource)
{
	downloadSemaphore.release();
	tryNextDownload();
	if (reply->error()) {
		qDebug() << "Download error" << reply->errorString();
		return;
	}
	auto file = std::make_unique<QTemporaryFile>();
	if (!file->open() || file->error()) {
		qDebug() << file->errorString();
		return;
	}
	file->write(reply->readAll());
	file->close();
	mediaSource->set_location(file->fileName());
	mediaSource->set_localFile(std::move(file));
}

void Downloader::tryNextDownload()
{
	if (downloadQueue.empty() || !downloadSemaphore.tryAcquire()) {
		return;
	}
	auto mediaSource = downloadQueue.front();
	downloadQueue.pop();
	download(mediaSource);
}
