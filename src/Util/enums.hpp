#pragma once

#include <QObject>

class Media : public QObject {
	Q_OBJECT
public:
	enum class Type : uint8_t {
		NONE,
		LINK,
		IMAGE, // also includes animated images such as "gif". Note that "gifv" files are not animated images, but "mp4" files (so MEDIA_VIDEO)
		ALBUM,
		VIDEO,
	};
	Q_ENUM(Type)
};

class Vote : public QObject {
	Q_OBJECT
public:
	/**
	 * @brief Represents a vote for an item
	 */
	enum class Direction : uint8_t {
		/** abstention */
		NONE,
		/** upvote */
		UP,
		/** downvote */
		DOWN
	};
	Q_ENUM(Direction)
};

class Collection : public QObject {
	Q_OBJECT
public:
	enum class Sorting : uint8_t {
		HOT,
		NEW,
		RISING,
		TOP,
		CONTROVERSIAL
	};
	Q_ENUM(Sorting)
};
