#pragma once

#define MEMBER_PROPERTY(TYPE, NAME) \
private: \
	TYPE m_##NAME; \
public: \
	TYPE get_##NAME() const { \
		return m_##NAME; \
	} \
	void set_##NAME(TYPE new_##NAME) { \
		m_##NAME = new_##NAME; \
	} \
TYPE* get_##NAME##_ptr() { \
		return &m_##NAME; \
	}

#define MEMBER_PROPERTY_INIT(TYPE, NAME, VALUE) \
private: \
	TYPE m_##NAME {VALUE}; \
public: \
	TYPE get_##NAME() const { \
		return m_##NAME; \
	} \
	void set_##NAME(TYPE new_##NAME) { \
		m_##NAME = new_##NAME; \
	}

#define MEMBER_SHARED_PROPERTY(TYPE, NAME) \
private: \
	std::shared_ptr<TYPE> m_##NAME; \
public: \
	TYPE* get_##NAME() const { \
		return m_##NAME.get(); \
	} \
	std::shared_ptr<TYPE>& get_##NAME##_shared() { \
		return m_##NAME; \
	} \
	void set_##NAME(std::shared_ptr<TYPE>& new_##NAME) { \
		m_##NAME = new_##NAME; \
	}
