#pragma once

#include <QQmlEngine>

#define IMPLEMENT_SHARED_LIST_MODEL(DATA_TYPE) \
public: \
	Q_INVOKABLE QVariant getData(const int i) const { \
		QQmlEngine::setObjectOwnership(m_data[i].get(), QQmlEngine::CppOwnership); \
		return QVariant::fromValue(m_data[i].get()); \
	} \
	virtual int rowCount(const QModelIndex&) const override { \
		return m_data.size(); \
	} \
	virtual QVariant data(const QModelIndex& index, int) const override { \
		return getData(index.row()); \
	} \
	virtual QHash<int, QByteArray> roleNames() const override { \
		return {{Qt::UserRole, "item"}}; \
	} \
	Q_INVOKABLE void append() { \
		const auto size = m_data.size(); \
		emit beginInsertRows(QModelIndex(), size, size); \
		m_data.push_back(std::make_shared<DATA_TYPE>()); \
		emit endInsertRows(); \
	} \
	Q_INVOKABLE void remove(const int i) { \
		emit beginRemoveRows(QModelIndex(), i, i); \
		m_data.erase(std::next(m_data.begin(), i)); \
		emit endRemoveRows(); \
	} \
private: \
	std::vector<std::shared_ptr<DATA_TYPE>> m_data;


#define SIMPLE_SHARED_LIST_MODEL(CLASS_NAME, DATA_TYPE) \
class CLASS_NAME : public QAbstractListModel \
{ \
	Q_OBJECT \
	IMPLEMENT_SHARED_LIST_MODEL(DATA_TYPE) \
};
