#pragma once

#include <QtNetwork>

#include "qmlpropertyhelper.hpp"
#include "propertyhelper.hpp"
#include "enums.hpp"

#define SINGLETON(TYPE) \
	static TYPE* get() { \
		static TYPE s; \
		return &s; \
	}

#if defined(Q_OS_LINUX) || defined(Q_OS_WIN) || defined(Q_OS_MAC)
#define OS_DESKTOP
#endif

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
#define OS_MOBILE
#endif

namespace Util {
	QNetworkAccessManager* getNetworkAccessManager();
	QString getTail(const QString& path, const bool removeExt = false);
	bool checkGenericError(const QJsonObject& obj);
	QString parseHtml(const QString& html);
	QJsonObject replyToJson(QNetworkReply* reply);

	// std algorithm wrappers
	/**
	 * @brief Accumulates all elements in a given container using the + operator
	 * @param cnt The container to accumulate elements from
	 * @param init The initial sum value
	 * @return The accumulated sum
	 */
	template<typename Cnt_T, typename T>
	T accumulate(Cnt_T &cnt, T init) {
		return std::accumulate(std::begin(cnt), std::end(cnt), init);
	}

	/**
	 * @brief Accumulates all elements in a given container using the + operator
	 * @param cnt The container to accumulate elements from
	 * @return The accumulated sum
	 */
	template<typename Cnt_T>
	auto accumulate(Cnt_T &cnt) {
		return Util::accumulate(cnt, 0);
	}

	/**
	 * @brief Finds an element in a container
	 * @param cnt The container to search in
	 * @param value The value to search for
	 * @return The iterator to the first fitting element or the end iterator if not found
	 */
	template<typename Cnt_T, typename T>
	auto find(Cnt_T &cnt, const T& value) {
		return std::find(std::begin(cnt), std::end(cnt), value);
	}

	/**
	 * @brief Finds out if an element is in a container
	 * @param cnt The container to search in
	 * @param value The value to search for
	 * @return True if \a cnt contains \a value
	 */
	template<typename Cnt_T, typename T>
	bool find_b(Cnt_T &cnt, const T& value) {
		return find(cnt, value) != std::end(cnt);
	}
}

constexpr size_t operator"" _kB (const unsigned long long s) {
	return 1000 * s;
}

constexpr size_t operator"" _MB (const unsigned long long s) {
	return 1000 * 1000 * s;
}

constexpr size_t operator"" _GB (const unsigned long long s) {
	return 1000 * 1000 * 1000 * s;
}
