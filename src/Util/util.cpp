#include "util.hpp"

static QJsonObject closedDevice = QJsonObject::fromVariantMap({{"error", 1}, {"message", "Device is closed"}});

QNetworkAccessManager* Util::getNetworkAccessManager()
{
	static QNetworkAccessManager singleton;
	return &singleton;
}

QString Util::getTail(const QString& path, const bool removeExt)
{
	QString tail = path.section('/', -1);
	if (removeExt) {
		tail = tail.section('.', 0, 0);
	}
	return tail;
}

/**
 * @brief Checks if a json object contains an error
 * @param obj The json object to check
 * @return True, if everything is fine
 */
bool Util::checkGenericError(const QJsonObject &obj)
{
	if (obj.contains("error")) {
		const auto error = obj["error"].toInt();
		const auto msg = obj["message"].toString();
		qDebug() << error << msg;
		return false;
	} else {
		return true;
	}
}

QString Util::parseHtml(const QString& html)
{
	QString result = html;
	// the HTML parser needs them parsed already
	// TODO: What about < and > that should not be parsed?
	static const std::map<QString, QString> replacements = {
		{"&lt;", "<"},
		{"&gt;", ">"},
	};
	for (auto& it : replacements) {
		result.replace(it.first, it.second);
	}
	return result;
}

QJsonObject Util::replyToJson(QNetworkReply* reply)
{
	if (!reply->isOpen()) {
		return closedDevice;
	}
	QByteArray responseData = reply->readAll();
	if (responseData.size() == 0) {
		return QJsonObject();
	}
	if (responseData[0] != '{') {
		responseData.prepend("{\"array\": ");
		responseData.append('}');
	}
	return QJsonDocument::fromJson(responseData).object();
}
