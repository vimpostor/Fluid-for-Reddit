#pragma once

#include <memory>
#include <QSettings>
#include <QDateTime>

#include "Util/util.hpp"

class Settings : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QString refreshToken READ getRefreshToken WRITE setRefreshToken)
	Q_PROPERTY(UserStatus userStatus READ getUserStatus WRITE setUserStatus NOTIFY userStatusChanged)
	Q_PROPERTY(bool openLinksExternally READ getOpenLinksExternally WRITE setOpenLinksExternally)
	Q_PROPERTY(bool confirmLinks READ getConfirmLinks WRITE setConfirmLinks)
	Q_PROPERTY(int mediaCacheLimit READ getMediaCacheLimit WRITE setMediaCacheLimit NOTIFY mediaCacheLimitChanged)
public:
	Settings() : QObject() {}

	enum UserStatus {
		SigningIn,
		UserLess,
		SignedIn,
	};
	Q_ENUM(UserStatus)

	SINGLETON(Settings)
	void init();

	QString getRefreshToken() const;
	void setRefreshToken(const QString& token);

	UserStatus getUserStatus() const;
	void setUserStatus(const UserStatus status);

	bool getOpenLinksExternally() const;
	void setOpenLinksExternally(bool s);

	bool getConfirmLinks() const;
	void setConfirmLinks(bool s);

	uint getMediaCacheLimit() const;
	void setMediaCacheLimit(const uint m);
signals:
	void userStatusChanged(Settings::UserStatus userStatus);
	void mediaCacheLimitChanged(uint mediaCacheLimit);
private:
	std::unique_ptr<QSettings> settings;
};
