#pragma once

constexpr bool isDefined(const char *str) {
	return *str != '\0';
}

#ifndef IMGUR_CLIENT_ID
#define IMGUR_CLIENT_ID ""
#endif // IMGUR_CLIENT_ID
static_assert(isDefined(IMGUR_CLIENT_ID), "You need to specify an imgur client id");

#ifndef REDDIT_CLIENT_ID
#define REDDIT_CLIENT_ID ""
#endif // REDDIT_CLIENT_ID
static_assert(isDefined(REDDIT_CLIENT_ID), "You need to specify a reddit client id");

#ifndef GFYCAT_CLIENT_ID
#define GFYCAT_CLIENT_ID ""
#endif // GFYCAT_CLIENT_ID
static_assert(isDefined(GFYCAT_CLIENT_ID), "You need to specify a gfycat client id");

#ifndef GFYCAT_CLIENT_SECRET
#define GFYCAT_CLIENT_SECRET ""
#endif // GFYCAT_CLIENT_SECRET
static_assert(isDefined(GFYCAT_CLIENT_ID), "You need to specify a gfycat client secret");

