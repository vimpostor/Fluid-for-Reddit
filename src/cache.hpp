#pragma once

#include <QSettings>
#include <memory>
#include <QCoreApplication>

#include "token.hpp"
#include "Api/Types/listing.hpp"
#include "Api/Types/subreddit.hpp"

class Cache : public QObject
{
	Q_OBJECT
public:
	SINGLETON(Cache)
	void init();

	Listing<Subreddit> getSubscriptions() const;
	void setSubscriptions(Listing<Subreddit>& sInfo);

	Token getToken(QString name) const;
	void setToken(const Token& t);
private:
	std::unique_ptr<QSettings> cache;
};
