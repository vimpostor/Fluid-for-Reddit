#pragma once

#include <QtNetwork>
#include <QDesktopServices>

#include "oauth.hpp"
#include "Api/Types/listing.hpp"
#include "Api/Types/link.hpp"
#include "Api/Types/comment.hpp"
#include "Api/Wrappers/subredditwrapper.hpp"
#include "Api/Wrappers/commentswrapper.hpp"
#include "gui.hpp"
#include "token.hpp"
#include "cache.hpp"
#include "apiresult.hpp"

// API endpoints, see https://www.reddit.com/dev/api for reference
#define GET_SUBREDDIT_ABOUT "r/%1/about"
#define GET_SUBREDDIT_SORT "r/%1/%2"
#define GET_SUBREDDITS_MINE_WHERE "subreddits/mine/%1"
#define GET_COMMENTS "comments/article"

#define POST_VOTE "api/vote"

class Api : public QObject
{
	Q_OBJECT
public:
	SINGLETON(Api)

	using CallbackT = std::function<void(QJsonObject)>;

	ApiResult<Listing<std::shared_ptr<Link>>>* getSubredditLinks(SubredditWrapper *subreddit);
	void getSubreddit(Subreddit* s);
	ApiResult<Listing<Subreddit> >* getSubscriptions();

	void getComments(Link* link);

	template<typename T>
	void postVote(const T votable, const Vote::Direction vote) {
		const auto currentVote = votable->get_votable()->get_likes();
		if (currentVote == vote) {
			// do not want to spam the same vote
			return;
		}
		QVariantMap parameters;
		parameters.insert("id", votable->get_thing()->get_name());
		parameters.insert("dir", QString::number(Votable::voteToDirection(vote)));
		auto reply = OAuth::get()->post(POST_VOTE, parameters);
		if (reply != nullptr) {
			connect(reply, &QNetworkReply::finished, this, std::bind(&Votable::processVote, votable->get_votable(), reply, vote));
		}
	}
	template<typename T>
	void upvote(const T votable) {
		postVote(votable, Vote::Direction::UP);
	}
	template<typename T>
	void downvote(const T votable) {
		postVote(votable, Vote::Direction::DOWN);
	}
	template<typename T>
	void unvote(const T votable) {
		postVote(votable, Vote::Direction::NONE);
	}
	template<typename T>
	void toggleUpvote(const T votable) {
		if (votable->get_votable()->get_likes() == Vote::Direction::UP) {
			unvote(votable);
		} else {
			upvote(votable);
		}
	}
	template<typename T>
	void toggleDownvote(const T votable) {
		if (votable->get_votable()->get_likes() == Vote::Direction::DOWN) {
			unvote(votable);
		} else {
			downvote(votable);
		}
	}
private:
	static void processSubscriptions(QJsonObject& data, ApiResult<Listing<Subreddit> >* reply);
	static void processSubredditLinks(QJsonObject& data, ApiResult<Listing<std::shared_ptr<Link>>>* reply);
};
