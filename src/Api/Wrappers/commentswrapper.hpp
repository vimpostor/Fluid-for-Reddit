#pragma once

#include "Api/Types/comment.hpp"

class CommentsWrapper : public QObject
{
	Q_OBJECT
public:
	explicit CommentsWrapper(QObject *parent = nullptr);
	Node<std::shared_ptr<Comment>> comments;
signals:
	void updated(Node<std::shared_ptr<Comment>> newComments);
public slots:
	void processComments();
private:
	void insertNode(Node<std::shared_ptr<Comment>>& parentNode, QJsonObject& o);
};
