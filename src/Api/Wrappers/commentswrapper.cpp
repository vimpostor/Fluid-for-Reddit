#include "commentswrapper.hpp"

CommentsWrapper::CommentsWrapper(QObject *parent) : QObject(parent)
{
}

void CommentsWrapper::processComments()
{
	auto rootObj = Util::replyToJson(dynamic_cast<QNetworkReply*>(sender()));
	auto rootArr = rootObj["array"].toArray();
	auto toplevelComments = rootArr[1].toObject()["data"].toObject()["children"].toArray();
	Node<std::shared_ptr<Comment>> result;
	for (auto i : toplevelComments) {
		auto o = i.toObject();
		insertNode(result, o);
	}
	emit updated(result);
}

void CommentsWrapper::insertNode(Node<std::shared_ptr<Comment> >& parentNode, QJsonObject& o)
{
	auto data = o["data"].toObject();
	auto c = std::make_shared<Comment>(data);
	auto n = Node(c);
	parentNode.children.push_back(n);
	auto& newParent = parentNode.children.children.back();
	auto replies = data["replies"].toObject()["data"].toObject()["children"].toArray();
	for (auto i : replies) {
		auto newChild = i.toObject();
		insertNode(newParent, newChild);
	}
}
