#pragma once

#include "Api/Types/link.hpp"
#include "Views/subredditview.hpp"

class SubredditWrapper : public QObject
{
	Q_OBJECT

	QML_OBJECT_PROPERTY(Subreddit, subreddit)
	QML_OBJECT_PROPERTY(SubredditView, subredditview)
public:
	explicit SubredditWrapper(QObject *parent = nullptr);

	Listing<std::shared_ptr<Link>> links;
	Collection::Sorting sorting {Collection::Sorting::HOT};
signals:
	void updated(Listing<std::shared_ptr<Link>> newLinks);
};
