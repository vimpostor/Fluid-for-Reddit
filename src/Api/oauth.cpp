#include "oauth.hpp"

#include <QSysInfo>

#include "secrets.hpp"

#ifdef OS_MOBILE
constexpr const char* OAUTH_AUTHORIZATION_URL = "https://www.reddit.com/api/v1/authorize.compact";
#else
constexpr const char* OAUTH_AUTHORIZATION_URL = "https://www.reddit.com/api/v1/authorize";
#endif

constexpr const char* OAUTH_ENDPOINT = "https://oauth.reddit.com/";
constexpr const char* OAUTH_TOKEN_URL = "https://www.reddit.com/api/v1/access_token";

OAuth::OAuth()
{
	const QString userAgent = QString("%1:%2:%3 (by u/vimpostor)").arg(QSysInfo::productType(), QCoreApplication::applicationName(), QCoreApplication::applicationVersion());
	oauth.setUserAgent(userAgent.toLocal8Bit());
	oauth.setNetworkAccessManager(Util::getNetworkAccessManager());
	oauth.setClientIdentifier(REDDIT_CLIENT_ID);
	oauth.setReplyHandler(&oauthReplyHandler);
	oauth.setAuthorizationUrl(QUrl(OAUTH_AUTHORIZATION_URL));
	oauth.setAccessTokenUrl(QUrl(OAUTH_TOKEN_URL));
	oauth.setScope("*");

	connect(&oauth, &QOAuth2AuthorizationCodeFlow::statusChanged, this, &OAuth::oauthStatusChanged);
	connect(&oauth, &QOAuth2AuthorizationCodeFlow::error, this, &OAuth::oauthServerError);
	connect(&oauth, &QOAuth2AuthorizationCodeFlow::requestFailed, this, &OAuth::handleFailedRequest);

	oauth.setModifyParametersFunction(QAbstractOAuth::ModifyParametersFunction{&OAuth::oauthModifyParameters});
	connect(&oauth, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser, Gui::get(), &Gui::openUrlDontAsk);
}

void OAuth::initialize()
{
	// check if the user already signed in
	if (Settings::get()->getUserStatus() == Settings::SignedIn) {
		// load the refresh token
		oauth.setRefreshToken(Settings::get()->getRefreshToken());
	} else if (Settings::get()->getUserStatus() == Settings::SigningIn) {
		// we are waiting for the user to sign in first
		return;
	}
	// check if the last access token is still valid
	accessToken = Cache::get()->getToken(accessToken.name);
	if (accessToken.isValid()) {
		oauth.setToken(accessToken.token);
	}
	setAccesstoken(accessToken.token, accessToken.expiration);
}

void OAuth::grant()
{
	if (Settings::get()->getUserStatus() == Settings::SigningIn) {
		emit Gui::get()->set_windowTitle("Waiting for callback...");
		oauth.grant();
	} else if (Settings::get()->getUserStatus() == Settings::UserLess) {
		refreshAccessToken();
	}
}

QNetworkReply* OAuth::get(const QString& link, const QVariantMap& parameters)
{
	QUrl url = OAUTH_ENDPOINT + link;
	qDebug() << "get" << url;
	auto* result = oauth.get(url, parameters);
	prepareNetworkReply(result);
	return result;
}

QNetworkReply* OAuth::post(const QString& link, const QVariantMap& parameters)
{
	QUrl url = OAUTH_ENDPOINT + link;
	qDebug() << "post" << url;
	auto* result = oauth.post(url, parameters);
	prepareNetworkReply(result);
	return result;
}

void OAuth::oauthStatusChanged(QAbstractOAuth::Status status)
{
	if (status == QAbstractOAuth::Status::Granted) {
		// authenticated, now save the refresh token
		if (!oauth.refreshToken().isEmpty()) {
			Settings::get()->setRefreshToken(oauth.refreshToken());
		}
		// save access token
		if (!oauth.token().isEmpty()) {
			setAccesstoken(oauth.token(), oauth.expirationAt());
		}
		Settings::get()->setUserStatus(Settings::SignedIn);
		emit Gui::get()->set_windowTitle("");
	}
}

void OAuth::oauthServerError(const QString& error, const QString& errorDescription, const QUrl& uri)
{
	qDebug() << error << errorDescription << uri;
}

void OAuth::handleFailedRequest()
{
	Gui::get()->set_snackbarText("The request failed!");
}

void OAuth::apiAbusalTimedOut()
{
	Gui::get()->set_snackbarText("The API can be used again");
}

void OAuth::processAnonymousToken(QNetworkReply* reply)
{
	auto rootObj = Util::replyToJson(reply);
	if (rootObj.contains("error")) {
		Gui::get()->set_snackbarText(rootObj["message"].toString());
		return;
	} else if (rootObj["token_type"].toString() != "bearer") {
		return;
	}
	if (rootObj.contains("access_token")) {
		emit Gui::get()->set_windowTitle("");
		setAccesstoken(rootObj["access_token"].toString(), QDateTime::currentDateTime().addSecs(rootObj["expires_in"].toInt()));
		oauth.setToken(accessToken.token);
	}
}

void OAuth::handleReplyError(const QNetworkReply* reply, QNetworkReply::NetworkError code)
{
	qDebug() << QString::number(code) + reply->errorString();
}

void OAuth::getAnonymousToken()
{
	QNetworkRequest req {QUrl(OAUTH_TOKEN_URL)};
	const QString credentials = QString(REDDIT_CLIENT_ID) + ":";
	const QString basicAuth = "Basic " + credentials.toLocal8Bit().toBase64();
	req.setRawHeader("Authorization", basicAuth.toLocal8Bit());
	req.setHeader(QNetworkRequest::UserAgentHeader, oauth.userAgent());
	req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
	const QByteArray data {"grant_type=https://oauth.reddit.com/grants/installed_client&device_id=DO_NOT_TRACK_THIS_DEVICE"};
	auto* reply = Util::getNetworkAccessManager()->post(req, data);
	prepareNetworkReply(reply);
	connect(reply, &QNetworkReply::finished, [=, this](){ processAnonymousToken(reply); });
}

void OAuth::processAnswerGeneral(const QNetworkReply* reply)
{
	int requestsRemaining = 1;
	if (reply->hasRawHeader("X-Ratelimit-Remaining")) {
		requestsRemaining = static_cast<int>(reply->rawHeader("X-Ratelimit-Remaining").toFloat());
	}
	const auto headerQuietTime = reply->rawHeader("X-Ratelimit-Reset").toInt();
	if (requestsRemaining <= 0) {
		QTimer::singleShot(headerQuietTime * 1000, this, &OAuth::apiAbusalTimedOut);
		Gui::get()->error(QString("You are exceeding the API limit. Please wait %1 seconds.").arg(headerQuietTime));
	}
}

void OAuth::oauthModifyParameters(QAbstractOAuth::Stage stage, QMultiMap<QString, QVariant>* parameters)
{
	if (stage == QAbstractOAuth::Stage::RequestingAuthorization) {
		parameters->insert("duration", "permanent");
	}
}

void OAuth::prepareNetworkReply(const QNetworkReply* reply)
{
	// usually a timer will automatically refresh the token before it's too late, but in case that this mechanism fails we check just to be sure that the token is still valid
	refreshAccessToken();
	connect(reply, &QNetworkReply::errorOccurred, [=, this](QNetworkReply::NetworkError code){ handleReplyError(reply, code); });
	connect(reply, &QNetworkReply::finished, [=, this](){ processAnswerGeneral(reply); });
}

void OAuth::refreshAccessToken()
{
	if (!accessToken.expiresSoon() || isRefreshingAccessToken) {
		// don't want to spam the servers
		return;
	}
	isRefreshingAccessToken = true;
	Gui::get()->set_windowTitle("Refreshing access token...");
	if (Settings::get()->getUserStatus() == Settings::SignedIn) {
		oauth.refreshAccessToken();
	} else if (Settings::get()->getUserStatus() == Settings::UserLess) {
		getAnonymousToken();
	}
}

void OAuth::setAccesstoken(const QString token, const QDateTime expiration)
{
	accessToken.token = token;
	accessToken.expiration = expiration;
	Cache::get()->setToken(accessToken);
	isRefreshingAccessToken = false;
	// schedule token refreshal
	QTimer::singleShot(std::max(0, accessToken.msecsToRecommendedRefreshal()), [=, this](){ refreshAccessToken(); });
}
