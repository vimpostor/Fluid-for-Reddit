#include "api.hpp"

ApiResult<Listing<std::shared_ptr<Link>>>* Api::getSubredditLinks(SubredditWrapper* subreddit)
{
	auto* result = new ApiResult<Listing<std::shared_ptr<Link>>>();
	const auto query = QString(GET_SUBREDDIT_SORT).arg(subreddit->get_subreddit()->get_display_name(), sortingToString(subreddit->sorting));
	QVariantMap parameters;
	QString after = subreddit->links.after;
	if (!after.isEmpty()) {
		parameters.insert("after", after);
	}
	auto* reply = OAuth::get()->get(query, parameters);
	if (reply != nullptr) {
		connect(reply, &QNetworkReply::finished, [=](){ auto data = Util::replyToJson(reply); processSubredditLinks(data, result); });
	}
	return result;
}

void Api::getSubreddit(Subreddit* s)
{
	const auto query = QString(GET_SUBREDDIT_ABOUT).arg(s->get_display_name());
	auto* reply = OAuth::get()->get(query);
//	if (reply != nullptr) {
//		connect(reply, &QNetworkReply::finished, s, &Subreddit::processSubreddit);
//	}
}

ApiResult<Listing<Subreddit>>* Api::getSubscriptions()
{
	constexpr const int subscriptionsFetchCount = 100;
	const auto query = QString(GET_SUBREDDITS_MINE_WHERE).arg("subscriber");
	QVariantMap parameters;
	parameters.insert("limit", subscriptionsFetchCount);
	auto* reply = OAuth::get()->get(query, parameters);
	auto* result = new ApiResult<Listing<Subreddit>>();
	if (reply != nullptr) {
		connect(reply, &QNetworkReply::finished, [=](){ auto data = Util::replyToJson(reply); processSubscriptions(data, result); });
	}
	return result;
}

void Api::getComments(Link* link)
{
	const auto id = link->get_thing().get_id();
	if (id.isEmpty()) {
		qDebug() << "comments id is empty";
		return;
	}
	const auto query = QString(GET_COMMENTS);
	QVariantMap parameters;
	parameters.insert("article", id);
	auto* reply = OAuth::get()->get(query, parameters);
//	if (reply != nullptr) {
//		connect(reply, &QNetworkReply::finished, link->get_comments(), &CommentsWrapper::processComments);
//	}
}

void Api::processSubscriptions(QJsonObject& data, ApiResult<Listing<Subreddit>>* reply)
{
	auto result = std::make_unique<Listing<Subreddit>>();
	auto subs = data["data"].toObject()["children"].toArray();
	for (auto sub : subs) {
		auto sInfo =Subreddit(sub.toObject()["data"].toObject());
		result->emplace_back(std::move(sInfo));
	}
	reply->set(std::move(result));
}

void Api::processSubredditLinks(QJsonObject& data, ApiResult<Listing<std::shared_ptr<Link>>>* reply)
{
	auto result = std::make_unique<Listing<std::shared_ptr<Link>>>();
	*result = SharedListing::make_listing<Link>(data);
	reply->set(std::move(result));
}
