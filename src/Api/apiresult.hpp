#pragma once

#include <QObject>
#include <QJsonObject>
#include <memory>

class ApiResultSignal : public QObject
{
	Q_OBJECT
public:
signals:
	/**
	 * @brief Emitted when the \c ApiResult is ready
	 *
	 * Note that the \c ApiResult instance is automatically deleted after this signal has been fired.
	 * Subscribers have to process the result immediately when this signal fires, by connecting to this signal.
	 * After that the instance is gone.
	 */
	void ready();
	void error(int code, std::string msg);
};

template<typename T>
class ApiResult : public ApiResultSignal
{
public:
	ApiResult() {
		connect(this, &ApiResultSignal::ready, this, &QObject::deleteLater);
		connect(this, &ApiResultSignal::error, this, &QObject::deleteLater);
	}
	void set(T&& result) {
		m_result = std::unique_ptr<T>(std::move(result));
		emit this->ready();
	}
	T&& get() {
		auto p = m_result.get();
		m_result.release();
		return std::move(*p);
	}
	void set(std::unique_ptr<T>&& result) {
		m_result = std::move(result);
		emit this->ready();
	}
	void markError(int errorCode = 0, std::string errorMessage = "") {
		emit this->error(errorCode, errorMessage);
	}
	/**
	 * @brief Checks if the provided Json contains errors
	 * @param data The Json to check for errors
	 * @return Returns \c True, if an error was found
	 *
	 * Automatically invalidates this object if an error was found.
	 */
	bool checkError(const QJsonObject& data) {
		if (data.contains("error")) {
			const auto error = data["error"].toInt();
			const auto msg = data["message"].toString().toStdString();
#ifdef QT_DEBUG
			qDebug() << error << QString::fromStdString(msg);
#endif
			markError(error, msg);
			return true;
		}
		return false;
	}
private:
	std::unique_ptr<T> m_result;
};

