#pragma once

#include "thing.hpp"

QString sortingToString(const Collection::Sorting s);

template<typename T>
class Listing
{
public:
	Listing() {}
	Listing(const QJsonObject& obj) {
		obj >> *this;
	}

	QString before;
	QString after;
	QString modhash;
	std::vector<T> children;

	unsigned size() const {
		return children.size();
	}
	const T& operator [](const unsigned i) const {
		return children[i];
	}
	T& operator [](const unsigned i) {
		return children[i];
	}
	void clear() {
		children.clear();
	}
	void push_back(const T& val) {
		children.push_back(val);
	}
	template <typename ...Params>
	void emplace_back(Params&&... args) {
		children.emplace_back(std::forward<Params>(args)...);
	}
	auto begin() { return children.begin(); }
	auto end() { return children.end(); }
	auto begin() const { return children.begin(); }
	auto end() const { return children.end(); }
	auto cbegin() const { return children.cbegin(); }
	auto cend() const { return children.cend(); }
	void appendListing(Listing<T>& l) {
		this->after = l.after;
		for (auto& i : l) {
			this->push_back(i);
		}
	}
};

template<typename T>
Listing<T>& operator >>(const QJsonObject& obj, Listing<T>& r) {
	QJsonObject root = obj["data"].toObject();
	r.before = root["before"].toString();
	r.after = root["after"].toString();
	QJsonArray tList = root["children"].toArray();
	for (auto i : tList) {
		T cmt(i.toObject()["data"].toObject());
		r.children.push_back(cmt);
	}
	return r;
}

namespace SharedListing {
	template<typename T>
	Listing<std::shared_ptr<T>> make_listing(const QJsonObject& obj) {
		Listing<std::shared_ptr<T>> result;
		QJsonObject root = obj["data"].toObject();
		result.before = root["before"].toString();
		result.after = root["after"].toString();
		QJsonArray tList = root["children"].toArray();
		for (auto i : tList) {
			auto element = std::make_shared<T>(i.toObject()["data"].toObject());
			result.children.push_back(element);
		}
		return result;
	}
}

template<typename T>
class Node
{
public:
	Node() {}
	Node(T data) {
		this->data = data;
	}

	T data;
	Listing<Node<T>> children;
	bool collapsed = false;

	unsigned size(const bool skipCollapsed = false) const {
		unsigned result = 1;
		if (skipCollapsed && this->collapsed) {
			return result;
		}
		for (const auto& c: children) {
			result += c.size();
		}
		return result;
	}
	unsigned childrenSize(const bool skipCollapsed = false) const {
		return this->size(skipCollapsed) - 1;
	}
	Node<T>& getFlattenedChildByIndex(int index, bool skipCollapsed = false) {
		int i = 0;
		for (; index >= children[i].size(skipCollapsed); index -= children[i].size(skipCollapsed), ++i) {
		}
		if (index == 0) {
			return children[i];
		} else {
			return children[i].getFlattenedChildByIndex(index - 1);
		}
	}
	const Node<T>& getFlattenedChildByIndex(int index, bool skipCollapsed = false) const {
		return getFlattenedChildByIndex(index, skipCollapsed);
	}
};
