#pragma once

#include "Api/Types/thing.hpp"

class Votable
{
public:
private:
	MEMBER_PROPERTY_INIT(uint, downs, 0)
	MEMBER_PROPERTY_INIT(uint, gilded, 0)
	MEMBER_PROPERTY_INIT(Vote::Direction, likes, Vote::Direction::NONE)
	MEMBER_PROPERTY_INIT(bool, saved, false)
	MEMBER_PROPERTY_INIT(int, score, 0)
	MEMBER_PROPERTY_INIT(uint, ups, 0)

public:
	Votable() = default;
	Votable(const QJsonObject& obj);

	static int voteToDirection(const Vote::Direction vote);
public slots:
	void processVote(QNetworkReply* reply, const Vote::Direction v);
};

Votable& operator >>(const QJsonObject& l, Votable& r);
