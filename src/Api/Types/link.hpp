#pragma once

#include "thing.hpp"
#include "votable.hpp"
#include "authored.hpp"
#include "moderatable.hpp"
#include "subreddit.hpp"
#include "Api/Media/mediasource.hpp"
#include "Api/Wrappers/commentswrapper.hpp"

class Link
{
	MEMBER_PROPERTY(Authored, authored)
	MEMBER_PROPERTY(Moderatable, moderatable)
	MEMBER_PROPERTY(Subreddit, subreddit)
	MEMBER_PROPERTY(Thing, thing)
	MEMBER_PROPERTY(Votable, votable)

	MEMBER_SHARED_PROPERTY(MediaSource, mediaSource)
	MEMBER_PROPERTY_INIT(bool, mediaSourceReady, false)
	MEMBER_SHARED_PROPERTY(MediaSource, thumbnailLocal)
	MEMBER_PROPERTY_INIT(bool, thumbnailReady, false)
	MEMBER_SHARED_PROPERTY(Link, crosspostParent)
	MEMBER_PROPERTY_INIT(bool, isCrosspost, false)

	MEMBER_PROPERTY_INIT(bool, clicked, false)
	MEMBER_PROPERTY(QString, domain)
	MEMBER_PROPERTY_INIT(bool, hidden, false)
	MEMBER_PROPERTY_INIT(bool, is_self, false)
	MEMBER_PROPERTY(QString, link_flair_css_class)
	MEMBER_PROPERTY(QString, link_flair_text)
	MEMBER_PROPERTY_INIT(bool, locked, false)
	MEMBER_PROPERTY_INIT(int, num_comments, 0)
	MEMBER_PROPERTY_INIT(bool, over_18, false)
	MEMBER_PROPERTY(QString, scrubber_media_url)
	MEMBER_PROPERTY(QString, thumbnail)
	MEMBER_PROPERTY(QString, title)
	MEMBER_PROPERTY(QUrl, url)
public:
	Link() = default;
	Link(const QJsonObject& obj);
};

Link& operator >>(const QJsonObject& l, Link& r);
