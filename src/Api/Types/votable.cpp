#include "votable.hpp"

Votable::Votable(const QJsonObject& obj)
{
	obj >> *this;
}

int Votable::voteToDirection(const Vote::Direction vote)
{
	switch (vote) {
	case Vote::Direction::DOWN: {
		return -1;
	}
	case Vote::Direction::NONE: {
		return 0;
	}
	case Vote::Direction::UP: {
		return 1;
	}
	}
	return 0;
}

void Votable::processVote(QNetworkReply *reply, const Vote::Direction v)
{
	auto rootObj = Util::replyToJson(reply);
	// TODO: Use ApiResult::checkError instead somewhere
	if (Util::checkGenericError(rootObj)) {
		// update the score
		const bool likedBefore = this->get_likes() == Vote::Direction::UP;
		const bool dislikedBefore = this->get_likes() == Vote::Direction::DOWN;
		const bool likesNow = v == Vote::Direction::UP;
		const bool dislikesNow = v == Vote::Direction::DOWN;
		set_ups(this->get_ups() - likedBefore + likesNow);
		set_downs(this->get_downs() - dislikedBefore + dislikesNow);
		set_score(this->get_score() - likedBefore + dislikedBefore + likesNow - dislikesNow);
		set_likes(v);
	}
}

Votable& operator >>(const QJsonObject& l, Votable& r)
{
	r.set_downs(l["downs"].toInt());
	r.set_gilded(static_cast<uint>(l["gilded"].toInt()));
	QVariant likes = l["likes"].toVariant();
	Vote::Direction temp_likes;
	if (likes.metaType().id() == QMetaType::Bool) {
		temp_likes = likes.toBool() ? Vote::Direction::UP : Vote::Direction::DOWN;
	} else {
		temp_likes = Vote::Direction::NONE;
	}
	r.set_likes(temp_likes);
	r.set_saved(l["saved"].toBool());
	r.set_score(l["score"].toInt());
	r.set_ups(l["ups"].toInt());
	return r;
}
