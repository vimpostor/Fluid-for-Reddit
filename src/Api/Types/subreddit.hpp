#pragma once

#include "thing.hpp"
#include "listing.hpp"

class Subreddit
{
	MEMBER_PROPERTY(Thing, thing)

	MEMBER_PROPERTY_INIT(uint, accounts_active, 0)
	MEMBER_PROPERTY_INIT(uint, active_user_count, 0)
	MEMBER_PROPERTY(QString, banner_img)
	MEMBER_PROPERTY(QDateTime, created)
	MEMBER_PROPERTY(QDateTime, created_utc)
	MEMBER_PROPERTY(QString, description)
	MEMBER_PROPERTY(QString, description_html)
	MEMBER_PROPERTY(QString, display_name)
	MEMBER_PROPERTY(QString, header_img)
	MEMBER_PROPERTY_INIT(bool, over18, false)
	MEMBER_PROPERTY(QString, public_description)
	MEMBER_PROPERTY(QString, public_description_html)
	MEMBER_PROPERTY(QString, subreddit)
	MEMBER_PROPERTY(QString, subreddit_id)
	MEMBER_PROPERTY(QString, subreddit_name_prefixed)
	MEMBER_PROPERTY(QString, subreddit_type)
	MEMBER_PROPERTY_INIT(uint, subscribers, 0)
	MEMBER_PROPERTY(QString, title)
	MEMBER_PROPERTY_INIT(bool, wiki_enabled, false)
public:
	Subreddit() = default;
	Subreddit(const QJsonObject& obj);

	void store(QSettings* settings);
	void restore(QSettings* settings);
};

Subreddit& operator >>(const QJsonObject& l, Subreddit& r);
bool operator !=(const Subreddit& l, const Subreddit& r);
bool operator <(const Subreddit& l, const Subreddit& r);
