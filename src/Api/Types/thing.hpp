#pragma once

#include <QString>
#include <list>
#include <QtNetwork>

#include "Util/util.hpp"

class Thing
{
	MEMBER_PROPERTY(QString, id)
	MEMBER_PROPERTY(QString, kind)
	MEMBER_PROPERTY(QString, name)
public:
	Thing() = default;
	Thing(const QJsonObject& obj);
};

Thing& operator >>(const QJsonObject& l, Thing& r);
bool operator !=(const Thing& l, const Thing& r);
