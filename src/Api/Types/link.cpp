#include "link.hpp"

Link::Link(const QJsonObject& obj)
	: m_authored (obj), m_moderatable (obj), m_subreddit (obj), m_thing (obj), m_votable (obj)
{
	obj >> *this;
}

Link& operator >>(const QJsonObject& l, Link& r)
{
	const auto crosspostParents = l["crosspost_parent_list"].toArray();
	if (!crosspostParents.isEmpty()) {
		auto crosspostParent = std::make_shared<Link>();
		crosspostParents[0].toObject() >> *crosspostParent.get();
		r.set_crosspostParent(crosspostParent);
		r.set_isCrosspost(true);
	}
	r.set_clicked(l["clicked"].toBool());
	r.set_domain(l["domain"].toString());
	r.set_hidden(l["hidden"].toBool());
	r.set_is_self(l["is_self"].toBool());
	r.set_link_flair_css_class(l["link_flair_css_class"].toString());
	r.set_link_flair_text(l["link_flair_text"].toString());
	r.set_locked(l["locked"].toBool());
	r.set_num_comments(l["num_comments"].toInt());
	r.set_over_18(l["over_18"].toBool());
	r.set_scrubber_media_url(l["media"].toObject()["reddit_video"].toObject()["fallback_url"].toString());
	if (r.get_scrubber_media_url().isEmpty() && r.get_isCrosspost()) {
		r.set_scrubber_media_url(r.get_crosspostParent()->get_scrubber_media_url());
	}
	QString thumbnail = l["thumbnail"].toString();
	if (thumbnail == "self" || thumbnail == "default" || thumbnail == "nsfw") {
		thumbnail = "";
	}
	r.set_thumbnail(thumbnail);
	r.set_title(l["title"].toString());
	r.set_url(QUrl(l["url"].toString()));
	if (!r.get_scrubber_media_url().isEmpty()) {
		r.set_url(r.get_scrubber_media_url());
	}
	return r;
}
