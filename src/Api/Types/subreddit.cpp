#include "subreddit.hpp"

Subreddit::Subreddit(const QJsonObject &obj)
{
	obj >> *this;
}

void Subreddit::store(QSettings* settings)
{
	QString className = "Subreddit";
	settings->beginGroup(className);
	settings->setValue("description", this->get_description_html());
	settings->setValue("display_name", this->get_display_name());
	settings->endGroup();
}

void Subreddit::restore(QSettings* settings)
{
	QString className = "Subreddit";
	settings->beginGroup(className);
	set_description_html(settings->value("description").toString());
	set_display_name(settings->value("display_name").toString());
	settings->endGroup();
}

//void Subreddit::processSubreddit()
//{
//	auto rootObj = Util::replyToJson(reply);
//	rootObj["data"].toObject() >> *this;
//}

Subreddit& operator >>(const QJsonObject& l, Subreddit& r)
{
	r.set_accounts_active(l["accounts_active"].toInt());
	r.set_active_user_count(l["active_user_count"].toInt());
	r.set_banner_img(l["banner_img"].toString());
	r.set_created(QDateTime::fromSecsSinceEpoch(l["created"].toVariant().toLongLong()));
	r.set_created_utc(QDateTime::fromSecsSinceEpoch(l["created_utc"].toVariant().toLongLong()));
	r.set_description(l["description"].toString());
	r.set_description_html(Util::parseHtml(l["description_html"].toString()));
	r.set_display_name(l["display_name"].toString());
	r.set_header_img(l["header_img"].toString());
	r.set_over18(l["over18"].toBool());
	r.set_public_description(l["public_description"].toString());
	r.set_public_description_html(l["public_description_html"].toString());
	r.set_subreddit(l["subreddit"].toString());
	r.set_subreddit_id(l["subreddit_id"].toString());
	r.set_subreddit_name_prefixed(l["subreddit_name_prefixed"].toString());
	r.set_subreddit_type(l["subreddit_type"].toString());
	r.set_subscribers(l["subscribers"].toInt());
	r.set_title(l["title"].toString());
	r.set_wiki_enabled(l["wiki_enabled"].toBool());
	return r;
}

bool operator !=(const Subreddit& l, const Subreddit& r)
{
	return l.get_subreddit_id() != r.get_subreddit_id();
}

bool operator <(const Subreddit &l, const Subreddit &r)
{
	return l.get_display_name().toLower() < r.get_display_name().toLower();
}
