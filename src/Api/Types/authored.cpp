#include "authored.hpp"

Authored::Authored(const QJsonObject& obj)
{
	obj >> *this;
}

Authored& operator >>(const QJsonObject& l, Authored & r)
{
	r.set_author(l["author"].toString());
	r.set_author_flair_css_class(l["author_flair_css_class"].toString());
	r.set_author_flair_text(l["author_flair_text"].toString());
	// some items save body as "body", some others as "selftext"
	if (l.contains("body")) {
		r.set_body(l["body"].toString());
		r.set_body_html(l["body_html"].toString());
	} else {
		r.set_body(l["selftext"].toString());
		r.set_body_html(l["selftext_html"].toString());
	}
	r.set_body(r.get_body());
	r.set_body_html(Util::parseHtml(r.get_body_html()));
	r.set_created(QDateTime::fromSecsSinceEpoch(l["created"].toVariant().toLongLong()));
	r.set_created_utc(QDateTime::fromSecsSinceEpoch(l["created_utc"].toVariant().toLongLong()));
	QVariant edited = l["edited"].toVariant();
	if (edited.metaType().id() == QMetaType::Bool) {
		r.set_edited(QDateTime());
	} else {
		r.set_edited(QDateTime::fromSecsSinceEpoch(edited.toLongLong()));
	}
	r.set_edited(r.get_edited());
	r.set_permalink(QUrl(l["permalink"].toString()));
	return r;
}

bool operator !=(const Authored& l, const Authored& r)
{
	Q_UNUSED(l)
	Q_UNUSED(r)
	return true;
}
