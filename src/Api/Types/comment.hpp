#pragma once

#include <vector>

#include "thing.hpp"
#include "votable.hpp"
#include "authored.hpp"
#include "moderatable.hpp"
#include "subreddit.hpp"

class Comment
{
	MEMBER_PROPERTY(Authored, authored)
	MEMBER_PROPERTY(Moderatable, moderatable)
	MEMBER_PROPERTY(Subreddit, subreddit)
	MEMBER_PROPERTY(Thing, thing)
	MEMBER_PROPERTY(Votable, votable)

	MEMBER_PROPERTY_INIT(bool, collapsed, false)
	MEMBER_PROPERTY_INIT(int, depth, 0)
public:
	Comment() = default;
	Comment(const QJsonObject& obj);
};

Comment& operator >>(const QJsonObject& l, Comment& r);
