#pragma once

#include "Api/Types/thing.hpp"

class Authored
{
	MEMBER_PROPERTY(QString, author)
	MEMBER_PROPERTY(QString, author_flair_css_class)
	MEMBER_PROPERTY(QString, author_flair_text)
	MEMBER_PROPERTY(QString, body)
	MEMBER_PROPERTY(QString, body_html)
	MEMBER_PROPERTY(QDateTime, created)
	MEMBER_PROPERTY(QDateTime, created_utc)
	MEMBER_PROPERTY(QDateTime, edited)
	MEMBER_PROPERTY(QUrl, permalink)
public:
	Authored() = default;
	Authored(const QJsonObject& obj);
};

Authored& operator >>(const QJsonObject& l, Authored& r);
bool operator !=(const Authored& l, const Authored& r);
