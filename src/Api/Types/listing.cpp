#include "listing.hpp"

QString sortingToString(const Collection::Sorting s)
{
	switch (s) {
	case Collection::Sorting::HOT:
		return "hot";
	case Collection::Sorting::NEW:
		return "new";
	case Collection::Sorting::RISING:
		return "rising";
	case Collection::Sorting::TOP:
		return "top";
	case Collection::Sorting::CONTROVERSIAL:
		return "controversial";
	}
	return "hot";
}
