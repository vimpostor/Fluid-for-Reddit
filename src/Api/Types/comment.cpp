#include "comment.hpp"

Comment::Comment(const QJsonObject& obj)
	: m_authored(obj), m_moderatable(obj), m_subreddit(obj), m_thing(obj), m_votable(obj)
{
	obj >> *this;
}

Comment& operator >>(const QJsonObject& l, Comment& r)
{
	r.set_collapsed(l["collapsed"].toBool());
	r.set_depth(l["depth"].toInt(0));
	return r;
}
