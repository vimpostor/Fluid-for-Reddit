#include "thing.hpp"

Thing::Thing(const QJsonObject& obj)
{
	obj >> *this;
}

Thing& operator >>(const QJsonObject& l, Thing& r)
{
	r.set_id(l["id"].toString());
	r.set_kind(l["kind"].toString());
	r.set_name(l["name"].toString());
	return r;
}

bool operator !=(const Thing& l, const Thing& r)
{
	return l.get_id() != r.get_id();
}
