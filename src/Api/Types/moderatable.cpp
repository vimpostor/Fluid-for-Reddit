#include "moderatable.hpp"

Moderatable::Moderatable(const QJsonObject& obj)
{
	obj >> *this;
}

Moderatable& operator >>(const QJsonObject& l, Moderatable& r)
{
	r.set_approved_at_utc(QDateTime::fromSecsSinceEpoch(l["approved_at_utc"].toVariant().toLongLong()));
	r.set_approved_by(l["approved_by"].toString());
	r.set_archived(l["archived"].toBool());
	r.set_banned_at_utc(QDateTime::fromSecsSinceEpoch(l["banned_at_utc"].toVariant().toLongLong()));
	r.set_banned_by(l["banned_by"].toString());
	r.set_distinguished(l["distinguished"].toBool());
	r.set_mod_note(l["mod_note"].toString());
	r.set_mod_reason_by(l["mod_reason_by"].toString());
	r.set_mod_reason_title(l["mod_reason_title"].toString());
	r.set_mod_reports(l["mod_reports"].toInt());
	r.set_num_reports(l["num_reports"].toInt());
	r.set_removal_reason(l["removal_reason"].toString());
	r.set_report_reason(l["report_reason"].toString());
	r.set_stickied(l["stickied"].toBool());
	r.set_user_reports(l["user_reports"].toInt());
	return r;
}

bool operator !=(const Moderatable& l, const Moderatable& r)
{
	Q_UNUSED(l)
	Q_UNUSED(r)
	return true;
}
