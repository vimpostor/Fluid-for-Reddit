#pragma once

#include "thing.hpp"

class Moderatable
{
	MEMBER_PROPERTY(QDateTime, approved_at_utc)
	MEMBER_PROPERTY(QString, approved_by)
	MEMBER_PROPERTY_INIT(bool, archived, false)
	MEMBER_PROPERTY(QDateTime, banned_at_utc)
	MEMBER_PROPERTY(QString, banned_by)
	MEMBER_PROPERTY_INIT(bool, distinguished, false)
	MEMBER_PROPERTY(QString, mod_note)
	MEMBER_PROPERTY(QString, mod_reason_by)
	MEMBER_PROPERTY(QString, mod_reason_title)
	MEMBER_PROPERTY_INIT(int, mod_reports, 0)
	MEMBER_PROPERTY_INIT(int, num_reports, 0)
	MEMBER_PROPERTY(QString, removal_reason)
	MEMBER_PROPERTY(QString, report_reason)
	MEMBER_PROPERTY_INIT(bool, stickied, false)
	MEMBER_PROPERTY_INIT(int, user_reports, 0)
public:
	Moderatable() = default;
	Moderatable(const QJsonObject& obj);
};

Moderatable& operator >>(const QJsonObject& l, Moderatable& r);
bool operator !=(const Moderatable& l, const Moderatable& r);
