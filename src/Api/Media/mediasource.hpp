#pragma once

#include <QObject>

#include "Util/util.hpp"

class AlbumModel;

/**
 * @brief Encapsulates media objects
 *
 * \a link stores the original link, e.g. https://i.imgur.com/timrsOb.gifv
 * \a type stores the media type, e.g. \c Media::Type::VIDEO
 * \a source stores the real download link, e.g. https://i.imgur.com/timrsOb.mp4
 * \a location stores the downloaded location, e.g. :///tmp/Fluid for Reddit.qNoYzc
 */
class MediaSource : public QObject
{
	Q_OBJECT

	QML_WRITABLE_VAR_PROPERTY(QUrl, link)
	QML_INIT_WRITABLE_VAR_PROPERTY(Media::Type, type, Media::Type::NONE)
	QML_WRITABLE_VAR_PROPERTY(QUrl, source)
	QML_WRITABLE_VAR_PROPERTY(QUrl, location)
	QML_INIT_WRITABLE_VAR_PROPERTY(bool, final, false)

	QML_INIT_WRITABLE_VAR_PROPERTY(int, bytesDownloaded, 0)
	QML_INIT_WRITABLE_VAR_PROPERTY(int, bytesTotal, 0)
	QML_INIT_READONLY_VAR_PROPERTY(float, downloadProgress, 0.f)
	QML_UNIQUE_PROPERTY(QTemporaryFile, localFile)

	QML_SHARED_PROPERTY(AlbumModel, albumModel)
public:
	explicit MediaSource(QUrl link = QUrl(), QObject* parent = nullptr);

	void finalizeSource();
	void finalizeSource(Media::Type type, QUrl source);
	void eraseDownload();
public slots:
	void processDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);
signals:
	void sourceFinalized();
};

bool operator==(const MediaSource& l, const QUrl& r);
bool operator==(const std::shared_ptr<MediaSource>& l, const QUrl& r);
bool operator==(const MediaSource& l, const MediaSource& r);
int operator+(const int& l, const MediaSource& r);
int operator+(const int& l, const std::shared_ptr<MediaSource>& r);
