#include "gfycatsourceextractor.hpp"

GfycatSourceExtractor::GfycatSourceExtractor()
{
	bearerToken = Cache::get()->getToken(bearerToken.name);
}

bool GfycatSourceExtractor::extractSource(const std::shared_ptr<MediaSource>& source)
{
	if (source->get_link().host() != "gfycat.com") {
		return false;
	}
	if (bearerToken.isValid()) {
		getGfycatInfo(source);
	} else {
		refreshToken(source);
	}
	return true;
}

void GfycatSourceExtractor::tokenFinished(QNetworkReply* reply, const std::shared_ptr<MediaSource>& source)
{
	QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
	QJsonObject rootObj = doc.object();
	if (rootObj.contains("access_token")) {
		bearerToken.token = rootObj["access_token"].toString();
		bearerToken.expiration = QDateTime::currentDateTime().addSecs(rootObj["expires_in"].toInt());
		Cache::get()->setToken(bearerToken);
		getGfycatInfo(source);
	} else {
		// something went wrong
		Gui::get()->set_snackbarText("Unable to retrieve a Gfycat token :(");
	}
}

void GfycatSourceExtractor::replyFinished(QNetworkReply* reply, const std::shared_ptr<MediaSource>& source)
{
	QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
	QJsonObject rootObj = doc.object();
	auto gfy = rootObj["gfyItem"].toObject();
	QString realSource;
	if (gfy.contains("mp4Url")) {
		realSource = gfy["mp4Url"].toString();
	} else if (gfy.contains("webmUrl")) {
		realSource = gfy["webmUrl"].toString();
	} else if (gfy.contains("gifUrl")) {
		realSource = gfy["gifUrl"].toString();
	} else {
		Gui::get()->set_snackbarText("Failed to get the gfycat source");
		return;
	}
	source->finalizeSource(Media::Type::VIDEO, realSource);
}

void GfycatSourceExtractor::refreshToken(const std::shared_ptr<MediaSource>& source)
{
	constexpr auto gfycatTokenEndpoint = "https://api.gfycat.com/v1/oauth/token";
	QNetworkRequest req {QUrl(gfycatTokenEndpoint)};
	req.setHeader(QNetworkRequest::ContentTypeHeader, QStringLiteral("application/x-www-form-urlencoded"));
	QJsonObject parameters;
	parameters["client_id"] = QString(GFYCAT_CLIENT_ID);
	parameters["client_secret"] = QString(GFYCAT_CLIENT_SECRET);
	parameters["grant_type"] = QString("client_credentials");
	QByteArray data = QJsonDocument(parameters).toJson(QJsonDocument::Compact);
	auto reply = Util::getNetworkAccessManager()->post(req, data);
	connect(reply, &QNetworkReply::finished, [=, this](){ tokenFinished(reply, source); });
}

void GfycatSourceExtractor::getGfycatInfo(const std::shared_ptr<MediaSource>& source)
{
	constexpr auto gfycatEndpoint = "https://api.gfycat.com/v1/gfycats/";
	QString id = Util::getTail(source->get_link().toString(), true);
	// remove hyphen suffix
	const auto hyphenPos = id.indexOf('-');
	if (hyphenPos != -1) {
		id.truncate(hyphenPos);
	}
	QNetworkRequest req(gfycatEndpoint + id);
	QByteArray header = (QString("Bearer ") + bearerToken.token).toUtf8();
	req.setRawHeader(QByteArray("Authorization"), header);
	auto reply = Util::getNetworkAccessManager()->get(req);
	connect(reply, &QNetworkReply::finished, [=, this](){ replyFinished(reply, source); });
}
