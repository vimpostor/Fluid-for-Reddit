#include "mediacache.hpp"

void MediaCache::push(const std::shared_ptr<MediaSource>& e)
{
	m_cache.emplace_front(e);
	// calculate the total size
	auto size = Util::accumulate(m_cache);
	const auto limit = Settings::get()->getMediaCacheLimit();
	while (size > limit) {
		// remove cached media until we have enough free memory
		const auto candidate = m_cache.back();
		size -= candidate->get_bytesTotal();
		candidate->eraseDownload();
		m_cache.pop_back();
	}
}

bool MediaCache::lookup(std::shared_ptr<MediaSource>& result, const QUrl& link)
{
	auto mediaSource = Util::find(m_cache, link);
	if (mediaSource != m_cache.end()) {
		// move to the beginning
		result = *mediaSource;
		m_cache.erase(mediaSource);
		m_cache.emplace_front(*mediaSource);
		return true;
	}
	return false;
}
