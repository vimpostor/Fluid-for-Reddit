#include "vredditsourceextractor.hpp"

bool VRedditSourceExtractor::extractSource(const std::shared_ptr<MediaSource>& source)
{
	if (source->get_link().host() != "v.redd.it") {
		return false;
	}
	Media::Type type = Media::Type::NONE;
	QString video = source->get_link().toString();
	if (!video.isEmpty()) {
		type = Media::Type::VIDEO;
	}
	source->finalizeSource(type, video);
	return true;
}
