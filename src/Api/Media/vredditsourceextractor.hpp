#pragma once

#include "mediasourceextractor.hpp"

class VRedditSourceExtractor : public MediaSourceExtractor
{
	Q_OBJECT
public:
	virtual bool extractSource(const std::shared_ptr<MediaSource>& source);
};
