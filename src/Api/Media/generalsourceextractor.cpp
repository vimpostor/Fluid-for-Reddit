#include "generalsourceextractor.hpp"

bool GeneralSourceExtractor::extractSource(const std::shared_ptr<MediaSource>& source)
{
	const std::vector<QString> imageMimetypes = {
		"jpeg",
		"jpg",
		"png",
		"gif",
	};
	const std::vector<QString> videoMimetypes = {
		"mp4",
	};
	const auto link = source->get_link();
	const auto hasMimetype = [&](const QString& mimetype) {
		return link.toString().endsWith("." + mimetype);
	};
	if (std::ranges::find_if(imageMimetypes, hasMimetype) != imageMimetypes.end()) {
		source->finalizeSource(Media::Type::IMAGE, link);
		return true;
	} else if (std::ranges::find_if(videoMimetypes, hasMimetype) != videoMimetypes.end()) {
		source->finalizeSource(Media::Type::VIDEO, link);
		return true;
	}
	return false;
}
