#include "linksourceextractor.hpp"

bool LinkSourceExtractor::extractSource(const std::shared_ptr<MediaSource>& source)
{
	if (source->get_link().isEmpty()) {
		return false;
	} else {
		source->finalizeSource(Media::Type::LINK, source->get_link());
		return true;
	}
}
