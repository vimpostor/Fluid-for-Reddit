#pragma once

#include <list>

#include "mediasource.hpp"
#include "settings.hpp"

class MediaCache
{
public:
	void push(const std::shared_ptr<MediaSource>& e);
	bool lookup(std::shared_ptr<MediaSource>& result, const QUrl& link);
private:
	// elements at the front are the most recent ones, older elements are at the end
	std::list<std::shared_ptr<MediaSource>> m_cache;
};
