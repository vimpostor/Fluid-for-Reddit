#include "mediasource.hpp"

MediaSource::MediaSource(QUrl link, QObject* parent) : QObject(parent)
{
	set_link(link);
	set_type(Media::Type::NONE);
	// when the download location is known, the MediaSource is viewable
	connect(this, &MediaSource::locationChanged, this, [&](const auto location){ this->set_final(!location.toString().isEmpty()); });
}

void MediaSource::finalizeSource()
{
	emit sourceFinalized();
	constexpr std::array<Media::Type, 3> instantlyReadyTypes = {Media::Type::NONE, Media::Type::LINK, Media::Type::ALBUM};
	if (Util::find_b(instantlyReadyTypes, get_type())) {
		set_final(true);
	}
}

void MediaSource::finalizeSource(Media::Type type, QUrl source)
{
	set_type(type);
	set_source(source);
	finalizeSource();
}

void MediaSource::eraseDownload()
{
	set_location({});
	m_localFile.reset();
	processDownloadProgress(0, 0);
}

void MediaSource::processDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	set_bytesDownloaded(bytesReceived);
	set_bytesTotal(bytesTotal);
	if (bytesTotal != 0) {
		set_downloadProgress(static_cast<float>(bytesReceived) / bytesTotal);
	}
}

bool operator==(const MediaSource& l, const QUrl& r)
{
	return l.get_link() == r;
}

bool operator==(const std::shared_ptr<MediaSource>& l, const QUrl& r)
{
	return *l == r;
}

bool operator==(const MediaSource& l, const MediaSource& r)
{
	return l.get_link() == r.get_link();
}

int operator+(const int& l, const MediaSource& r)
{
	return l + r.get_bytesTotal();
}

int operator+(const int& l, const std::shared_ptr<MediaSource>& r)
{
	return l + *r.get();
}
