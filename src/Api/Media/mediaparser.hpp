#pragma once

#include <QObject>
#include <memory>

#include "Api/Types/link.hpp"
#include "downloader.hpp"
#include "gui.hpp"
#include "generalsourceextractor.hpp"
#include "vredditsourceextractor.hpp"
#include "gfycatsourceextractor.hpp"
#include "imgursourceextractor.hpp"
#include "linksourceextractor.hpp"
#include "mediacache.hpp"
#include "Views/linkview.hpp"

class MediaParser : public QObject
{
	Q_OBJECT
public:
	explicit MediaParser(QObject *parent = nullptr);
	SINGLETON(MediaParser)

	Q_INVOKABLE std::shared_ptr<MediaSource> parseLink(Link* link, bool openWhenFinished = false);
	Q_INVOKABLE std::shared_ptr<MediaSource> parseLink(const QUrl& link, bool openWhenFinished = false);
	Q_INVOKABLE void openLink(LinkView* link);
	void openLink(Link* link);
	Q_INVOKABLE void openUrl(const QUrl& link);
	Q_INVOKABLE void getThumbnail(LinkView* link);
	void getThumbnail(Link* link);
private slots:
	void processMediaSource(const std::shared_ptr<MediaSource>& mediaSource);
private:
	std::vector<std::unique_ptr<MediaSourceExtractor>> m_extractors;
	MediaCache m_cache;
};
