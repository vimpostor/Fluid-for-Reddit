#pragma once

#include <QObject>
#include <memory>

#include "Util/util.hpp"
#include "secrets.hpp"
#include "cache.hpp"
#include "gui.hpp"

class MediaSourceExtractor : public QObject
{
	Q_OBJECT
public:
	explicit MediaSourceExtractor(QObject *parent = nullptr);

	virtual bool extractSource(const std::shared_ptr<MediaSource>& source);
};
