#include "imgursourceextractor.hpp"


bool ImgurSourceExtractor::extractSource(const std::shared_ptr<MediaSource>& source)
{
	const auto domain = source->get_link().host();
	if (domain != "i.imgur.com" && domain != "imgur.com") {
		return false;
	}
	const QString imgurId = Util::getTail(source->get_link().toString(), true);
	constexpr auto imgurImageEndpoint = "https://api.imgur.com/3/image/";
	constexpr auto imgurAlbumEndpoint = "https://api.imgur.com/3/album/";
	QString endpoint = imgurImageEndpoint;
	const QString imgurType = source->get_link().toString().section('/', -2, -2);
	if (imgurType == "a" || imgurType == "gallery") {
		// album
		endpoint = imgurAlbumEndpoint;
	}
	QNetworkRequest networkRequest(endpoint + imgurId);
	QByteArray header = (QString("Client-ID ") + QString(IMGUR_CLIENT_ID)).toUtf8();
	networkRequest.setRawHeader(QByteArray("authorization"), header);
	auto reply = Util::getNetworkAccessManager()->get(networkRequest);
	connect(reply, &QNetworkReply::finished, [=, this](){ replyFinished(reply, source); });
	return true;
}

void ImgurSourceExtractor::replyFinished(QNetworkReply* reply, const std::shared_ptr<MediaSource>& source)
{
	QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
	QJsonObject rootObj = doc.object();
	QJsonObject data = rootObj["data"].toObject();
	bool success = rootObj["success"].toBool();
	if (!success) {
		qDebug() << "Error: " + data["error"].toString();
		return;
	}
	if (data.contains("is_album") && data["is_album"].toBool()) {
		// album
		const auto images = data["images"].toArray();
		auto albumModel = std::make_shared<AlbumModel>();
		for (const auto& img : images) {
			auto imgObj = img.toObject();
			auto mediaSource = std::make_shared<MediaSource>();
			if (parseImgurLink(img.toObject(), *mediaSource)) {
				mediaSource->set_link(mediaSource->get_source());
				albumModel->appendChild(mediaSource);
			}
		}
		source->set_albumModel(albumModel);
		source->set_type(Media::Type::ALBUM);
		source->set_source(data["link"].toString());
	} else {
		// single link
		if (!parseImgurLink(data, *(source.get()))) {
			return;
		}
	}
	source->finalizeSource();
}

bool ImgurSourceExtractor::parseImgurLink(const QJsonObject& json, MediaSource& mediaSource)
{
	QString imgurType = json["type"].toString();
	if (imgurType == "image/gif" || imgurType == "video/mp4") {
		mediaSource.set_type(Media::Type::VIDEO);
		mediaSource.set_source(json["mp4"].toString());
	} else if (imgurType == "image/jpeg" || imgurType == "image/png") {
		mediaSource.set_type(Media::Type::IMAGE);
		mediaSource.set_source(json["link"].toString());
	} else {
		return false;
	}
	return true;
}
