#pragma once

#include "mediasourceextractor.hpp"
#include "Models/albummodel.hpp"

class ImgurSourceExtractor : public MediaSourceExtractor
{
	Q_OBJECT
public:
	virtual bool extractSource(const std::shared_ptr<MediaSource>& source);
private:
	void replyFinished(QNetworkReply* reply, const std::shared_ptr<MediaSource>& source);
	bool parseImgurLink(const QJsonObject& json, MediaSource& mediaSource);
};
