#include "mediasourceextractor.hpp"

MediaSourceExtractor::MediaSourceExtractor(QObject *parent) : QObject(parent)
{
}

bool MediaSourceExtractor::extractSource(const std::shared_ptr<MediaSource>& source)
{
	source->finalizeSource(Media::Type::NONE, QUrl());
	return true;
}
