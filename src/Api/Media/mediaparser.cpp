#include "mediaparser.hpp"

#include <utility>

MediaParser::MediaParser(QObject *parent) : QObject(parent)
{
	m_extractors.emplace_back(std::make_unique<GeneralSourceExtractor>());
	m_extractors.emplace_back(std::make_unique<VRedditSourceExtractor>());
	m_extractors.emplace_back(std::make_unique<GfycatSourceExtractor>());
	m_extractors.emplace_back(std::make_unique<ImgurSourceExtractor>());
	m_extractors.emplace_back(std::make_unique<LinkSourceExtractor>());
	m_extractors.emplace_back(std::make_unique<MediaSourceExtractor>());
}

std::shared_ptr<MediaSource> MediaParser::parseLink(Link* link, bool openWhenFinished)
{
	auto result = parseLink(link->get_url(), openWhenFinished);
	link->set_mediaSource(result);
	link->set_mediaSourceReady(true);
	return result;
}

std::shared_ptr<MediaSource> MediaParser::parseLink(const QUrl& link, bool openWhenFinished)
{
	std::shared_ptr<MediaSource> result;
	// first look it up in the cache
	const auto cacheHit = m_cache.lookup(result, link);
	if (!cacheHit) {
		// cache miss
		result = std::make_shared<MediaSource>(link);
		connect(result.get(), &MediaSource::sourceFinalized, this, [=, this](){ this->processMediaSource(result); });
		for (auto &i : m_extractors) {
			if (i->extractSource(result)) {
				break;
			}
		}
		m_cache.push(result);
	}

	if (openWhenFinished) {
		if (result->get_final()) {
			Gui::get()->openMedium(result);
		} else {
			// open when it is ready
			connect(result.get(), &MediaSource::finalChanged, this, [=](){ Gui::get()->openMedium(result); }, Qt::ConnectionType::SingleShotConnection);
		}
	}
	return result;
}

void MediaParser::openLink(LinkView* link)
{
	openLink(link->get_object());
}

void MediaParser::openLink(Link* link)
{
	parseLink(link, true);
}

void MediaParser::openUrl(const QUrl& link)
{
	parseLink(link, true);
}

void MediaParser::getThumbnail(LinkView* link)
{
	getThumbnail(link->get_object());
	emit link->thumbnailReadyChanged();
}

void MediaParser::getThumbnail(Link* link)
{
	auto result = parseLink(link->get_thumbnail());
	link->set_thumbnailLocal(result);
	link->set_thumbnailReady(true);
}

void MediaParser::processMediaSource(const std::shared_ptr<MediaSource>& mediaSource)
{
	const auto type = mediaSource->get_type();
	if (type == Media::Type::IMAGE || type == Media::Type::VIDEO) {
		Downloader::get()->queueDownload(mediaSource);
	}
}
