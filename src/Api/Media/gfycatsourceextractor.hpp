#pragma once

#include "mediasourceextractor.hpp"
#include "token.hpp"

class GfycatSourceExtractor : public MediaSourceExtractor
{
	Q_OBJECT
public:
	explicit GfycatSourceExtractor();

	virtual bool extractSource(const std::shared_ptr<MediaSource>& source);
private:
	void tokenFinished(QNetworkReply* reply, const std::shared_ptr<MediaSource>& source);
	void replyFinished(QNetworkReply* reply, const std::shared_ptr<MediaSource>& source);
	void refreshToken(const std::shared_ptr<MediaSource>& source);
	void getGfycatInfo(const std::shared_ptr<MediaSource>& source);

	Token bearerToken{"gfycat"};
};
