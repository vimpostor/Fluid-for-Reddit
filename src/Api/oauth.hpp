#pragma once

#include <QtNetworkAuth>

#include "Util/util.hpp"
#include "settings.hpp"
#include "token.hpp"
#include "cache.hpp"
#include "gui.hpp"

class OAuth : public QObject
{
	Q_OBJECT
public:
	OAuth();
	SINGLETON(OAuth)

	void initialize();
	void grant();
	QNetworkReply* get(const QString& link, const QVariantMap& parameters = QVariantMap());
	QNetworkReply* post(const QString& link, const QVariantMap& parameters = QVariantMap());

private slots:
	void oauthStatusChanged(QAbstractOAuth::Status status);
	void oauthServerError(const QString &error, const QString &errorDescription, const QUrl &uri);
	void handleFailedRequest();
	void apiAbusalTimedOut();
private:
	void processAnonymousToken(QNetworkReply* reply);
	void handleReplyError(const QNetworkReply* reply, QNetworkReply::NetworkError code);
	void getAnonymousToken();
	void processAnswerGeneral(const QNetworkReply* reply);
	static void oauthModifyParameters(QAbstractOAuth::Stage stage, QMultiMap<QString, QVariant>* parameters);
	void prepareNetworkReply(const QNetworkReply* reply);
	void refreshAccessToken();
	void setAccesstoken(const QString token, const QDateTime expiration);

	QOAuth2AuthorizationCodeFlow oauth;
	QOAuthHttpServerReplyHandler oauthReplyHandler{1337};
	Token accessToken{"reddit"};
	bool isRefreshingAccessToken = false;
};
