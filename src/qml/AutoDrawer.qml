import QtQuick
import QtQuick.Controls

Drawer {
	width: root.inPortrait ? 0.66 * parent.width : 256
	height: root.height
	modal: root.inPortrait
	interactive: root.inPortrait
	position: root.inPortrait ? 0 : 1
	visible: !root.inPortrait
}
