import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

import Backend

ApplicationWindow {
	id: root
	visible: true
	width: 1200
	height: 900
	Material.theme: Material.System
	Material.primary: Material.Blue
	Material.accent: Material.Orange
	title: c_gui.windowTitle
	readonly property bool inPortrait: width < height
	Component.onCompleted: {
		if (c_settings.userStatus === Settings.SigningIn) {
			signinDialog.open();
		}
	}
	Connections {
		target: c_gui
		function onConfirmOpenUrl(url) {
			openUrlDialog.url = url;
			openUrlDialog.open();
		}
		function onOpenImage(mediaSource) { inlineView.openMedium(mediaSource); }
		function onOpenVideo(mediaSource) { inlineView.openMedium(mediaSource); }
		function onOpenAlbum(mediaSource) { inlineView.openMedium(mediaSource); }
	}
	SubscriptionsDrawer {
		id: subscriptionsDrawer
		focus: false
		parent: parent
	}
	MainTabPage {
		id: mainTabPage
		anchors {fill: parent; leftMargin: subscriptionsDrawer.width * subscriptionsDrawer.position}
	}
	InlineView {
		id: inlineView
	}
	ConfirmUrlDialog {
		id: openUrlDialog
	}
	SigninDialog {
		id: signinDialog
	}
	Snackbar {
		id: mainSnackbar
		text: c_gui.snackbarText
		onClosed: c_gui.snackbarText = ""
		onTextChanged: if (text !== "") post();
	}
}
