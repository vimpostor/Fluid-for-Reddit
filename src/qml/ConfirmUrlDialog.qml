import QtQuick
import QtQuick.Controls

Dialog {
	id: root
	property url url: ""
	x: (parent.width - width) / 2
	y: (parent.height - height) / 2
	width: 400
	title: "Open Browser"
	onAccepted: c_gui.openUrlDontAsk(url);
	Label {
		text: "This will open " + root.url + " in your web browser."
		anchors.left: parent.left
		anchors.right: parent.right
		wrapMode: Text.WordWrap
	}
	standardButtons: Dialog.Cancel | Dialog.Ok
}
