import QtQuick
import QtQuick.Controls

import Backend

Item {
	required property MediaSource currentMediaSource
	ListView {
		anchors.fill: parent
		model: currentMediaSource.albumModel
		delegate: Button {
			text: mediaSource.source
			onClicked: c_mediaParser.openUrl(mediaSource.source);
		}
	}
}
