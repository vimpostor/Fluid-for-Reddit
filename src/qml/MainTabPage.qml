import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item {
	property int currentIndex: tabBar.currentIndex
	property var currentLinks: tabRepeater.itemAt(currentIndex).item
	TabBar {
		id: tabBar
		anchors {left: parent.left; right: addTabButton.left}
		Repeater {
			id: tabBarRepeater
			model: c_tabmodel
			delegate: TabButton {
				text: delegate_item === "LinkmodelDelegate.qml" ? delegate_model.subredditWrapper.subredditview.display_name === "" ? "New Tab" : delegate_model.subredditWrapper.subredditview.display_name : "Tab"
				width: implicitWidth + (closeButton.visible ? closeButton.width : 0)
				focusPolicy: Qt.NoFocus
				Behavior on width {
					NumberAnimation {
						duration: 200
						easing.type: Easing.InOutSine
					}
				}
				ToolButton {
					id: closeButton
					anchors.right: parent.right
					focusPolicy: Qt.NoFocus
					icon.source: "qrc:/navigation/close"
					visible: currentIndex === index
					onClicked: c_tabmodel.remove(index);
				}
			}
		}
	}
	Repeater {
		id: tabRepeater
		model: c_tabmodel
		Loader {
			anchors {left: parent.left; right: parent.right; top: tabBar.bottom; bottom: parent.bottom}
			focus: true
			source: delegate_item
			visible: tabBar.currentIndex === index
		}
	}
	RoundButton {
		id: addTabButton
		icon.source: "qrc:/content/add"
		anchors.right: parent.right
		onClicked: c_tabmodel.append();
		focusPolicy: Qt.NoFocus
	}
}
