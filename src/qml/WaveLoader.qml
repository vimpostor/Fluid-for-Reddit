import QtQuick

Loader {
	property point initialPos: Qt.point(0, 0)
	property bool open: false
	property var waveChildren: Item {}

	id: waveLoaderRoot
	sourceComponent: open ? loaderComponent : undefined
	Component {
		id: loaderComponent
		Wave {
			id: wave
			initialX: initialPos.x
			initialY: initialPos.y
			open: waveLoaderRoot.open
			anchors.fill: parent
			children: waveLoaderRoot.waveChildren
		}
	}
}
