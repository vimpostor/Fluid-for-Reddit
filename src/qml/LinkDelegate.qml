import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

import Backend

Item {
	function openMedium() {
		c_mediaParser.openLink(link);
	}
	focus: true
	width: grid.cellWidth
	height: grid.cellHeight
	Keys.onPressed: (event)=> {
		if (event.key === Qt.Key_K) {
			grid.moveCurrentIndexUp();
		} else if (event.key === Qt.Key_J) {
			grid.moveCurrentIndexDown();
		} else if (event.key === Qt.Key_H) {
			grid.moveCurrentIndexLeft();
		} else if (event.key === Qt.Key_L) {
			grid.moveCurrentIndexRight();
		} else if (event.key === Qt.Key_Return) {
			openMedium();
		}
	}
	Card {
		anchors.fill: parent
		anchors.margins: 8
		focus: GridView.isCurrentItem
		Rectangle {
			anchors.fill: parent
			color: Material.primary
			visible: GridView.isCurrentItem
		}
		MouseArea {
			anchors.fill: parent
			onClicked: {
				grid.currentIndex = index;
			}
			ProgressBar {
				anchors { left: parent.left; bottom: parent.bottom; right: parent.right }
				visible: value !== 0 && value !== 1
				value: link.mediaSourceReady ? link.mediaSource.downloadProgress : 0
			}
		}
		Item {
			id: cardColumn
			anchors.fill: parent
			Label {
				id: titleLabel
				anchors.left: parent.left
				anchors.right: parent.right
				anchors.top: parent.top
				wrapMode: Text.WordWrap
				text: link.title
			}
			Image {
				anchors.left: parent.left
				anchors.right: parent.right
				anchors.top: titleLabel.bottom
				anchors.bottom: buttonsRow.top
				source: link.thumbnailReady ? (link.thumbnailLocal.location != "" ? ("file://" + link.thumbnailLocal.location) : "qrc:/action/language") : "qrc:/action/language"
				fillMode: Image.PreserveAspectCrop
				Component.onCompleted: c_mediaParser.getThumbnail(link);
				MouseArea {
					anchors.fill: parent
					onClicked: {
						grid.currentIndex = index;
						openMedium();
					}
				}
			}
			Row {
				id: buttonsRow
				anchors.bottom: parent.bottom
				ToolButton {
					icon.source: "qrc:/navigation/arrow_upward"
					icon.color: link.votable.likes === Vote.UP ? Material.accent : "transparent"
					enabled: c_backend.isLoggedIn
				}
				Label {
					text: link.votable.ups
					anchors.verticalCenter: parent.verticalCenter
				}
				ToolButton {
					icon.source: "qrc:/navigation/arrow_downward"
					icon.color: link.votable.likes === Vote.DOWN ? Material.accent : "transparent"
					enabled: c_backend.isLoggedIn
				}
				ToolButton {
					icon.source: "qrc:/communication/comment"
					onClicked: c_backend.openComments(link);
				}
				Label {
					text: link.num_comments
					anchors.verticalCenter: parent.verticalCenter
				}
			}
			ToolButton {
				anchors.bottom: parent.bottom
				anchors.right: parent.right
				icon.source: "qrc:/navigation/more_vert"
				onClicked: linkMenu.popup();
				Menu {
					id: linkMenu
					Action {
						text: "Copy URL"
						icon.source: "qrc:/editor/insert_link"
						onTriggered: c_gui.copyToClipboard(link.url);
					}
					Action {
						text: "Copy parsed URL"
						icon.source: "qrc:/image/image"
						onTriggered: c_gui.copyToClipboard(link.mediaSource.source);
					}
					Action {
						text: "Copy comments"
						icon.source: "qrc:/communication/comment"
						onTriggered: c_gui.copyToClipboard(link.authored.permalink);
					}
					Action {
						text: "Open externally"
						icon.source: "qrc:/action/open_in_browser"
						onTriggered: c_gui.openUrl(link.url);
					}
					Action {
						text: "Open in new Tab"
						icon.source: "qrc:/content/add"
						onTriggered: c_backend.openNewMediasourceTab(link);
					}
				}
			}
		}
	}
}
