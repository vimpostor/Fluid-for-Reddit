import QtQuick

import Backend

Item {
	id: mediasourceViewRoot
	property MediaSource currentMediaSource: MediaSource {}
	Loader {
		anchors.fill: parent
		sourceComponent: currentMediaSource.type === Media.IMAGE ? imageView : (currentMediaSource.type === Media.VIDEO ? videoView : albumView)
	}
	Component {
		id: imageView
		ImageView {
			currentMediaSource: mediasourceViewRoot.currentMediaSource
		}
	}
	Component {
		id: videoView
		VideoView {
			currentMediaSource: mediasourceViewRoot.currentMediaSource
		}
	}
	Component {
		id: albumView
		AlbumView {
			currentMediaSource: mediasourceViewRoot.currentMediaSource
		}
	}
}
