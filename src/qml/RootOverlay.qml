import QtQuick
import QtQuick.Controls

Popup {
	enter: Transition {}
	exit: Transition {}
	width: root.width
	height: root.height
	anchors.centerIn: parent
}
