import QtQuick
import QtQuick.Controls

import Backend

Dialog {
	x: (parent.width - width) / 2
	y: (parent.height - height) / 2
	width: 400
	title: "Sign in with your Reddit account"
	standardButtons: Dialog.No | Dialog.Ok
	onAccepted: c_backend.setUserStatus(Settings.SigningIn);
	onRejected: c_backend.setUserStatus(Settings.UserLess);
	Label {
		text: "Sign in with your Reddit account so that you can vote and comment on posts."
		anchors.left: parent.left
		anchors.right: parent.right
		wrapMode: Text.WordWrap
	}
}
