import QtQuick
import QtQuick.Controls

Item {
	ListView {
		anchors.fill: parent
		model: delegate_model
		spacing: 8
		delegate: Card {
			x: comment.depth * 8
			width: parent.width - x
			MarkdownLabel {
				width: parent.width
				text: comment.authored.body
			}
		}
	}
}
