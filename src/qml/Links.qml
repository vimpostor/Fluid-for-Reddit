import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Material

import Backend

Item {
	id: linksView
	property alias grid: grid
	property alias model: grid.model
	property int count: grid.count
	BusyIndicator {
		anchors.centerIn: parent
		running: grid.model.loading
	}
	GridView {
		id: grid
		property int columns: width / cellWidth
		focus: true
		anchors.fill: parent
		cellWidth: 400
		cellHeight: 200
		clip: true
		anchors.margins: 8
		function openLeft() {
			moveCurrentIndexLeft();
			currentItem.openMedium();
			model.preloadMedium(currentIndex, true);
		}
		function openRight() {
			moveCurrentIndexRight();
			currentItem.openMedium();
			model.preloadMedium(currentIndex, false);
		}
		onCurrentIndexChanged: {
			if (currentIndex > 0 && currentIndex >= count - columns) {
				model.requestMoreResults();
			}
			positionViewAtIndex(currentIndex, GridView.Contain);
		}
		onContentYChanged: {
			if (atYEnd) {
				model.requestMoreResults();
			}
		}
		delegate: LinkDelegate {}
		highlight: Rectangle {
			color: Material.primary
			radius: 8
		}
	}
}
