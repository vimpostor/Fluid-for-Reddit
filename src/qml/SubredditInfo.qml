import QtQuick
import QtQuick.Controls

import Backend

Rectangle {
	property Subreddit qmlSubreddit
	ScrollView {
		anchors.fill: parent
		Label {
			text: qmlSubreddit === null ? "" : qmlSubreddit.description_html
			wrapMode: Text.WordWrap
			textFormat: Qt.RichText
		}
	}
	Button {
		text: "Close"
		anchors.bottom: parent.bottom
		anchors.horizontalCenter: parent.horizontalCenter
		onClicked: parent.visible = false;
	}
}
