import QtQuick
import QtQuick.Controls

import Backend

AnimatedImage {
	required property MediaSource currentMediaSource
	fillMode: Image.PreserveAspectFit
	visible: currentMediaSource.location !== ""
	source: "file://" + currentMediaSource.location
	cache: false
	onStatusChanged: playing = (status === AnimatedImage.Ready);
}
