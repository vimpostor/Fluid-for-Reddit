import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

Popup {
	property alias text: snackbarLabel.text
	function post() {
		open();
	}

	property int offset: 0

	id: snackbarRoot
	parent: Overlay.overlay
	x: (parent.width - width) / 2
	y: parent.height - offset
	z: 1
	closePolicy: Popup.NoAutoClose
	Material.theme: Material.Dark
	modal: false
	enter: Transition {
		NumberAnimation {
			property: "offset"
			duration: 200
			easing.type: Easing.InOutSine
			from: 0
			to: implicitHeight + 16
		}
	}
	exit: Transition {
		NumberAnimation {
			property: "offset"
			duration: 200
			easing.type: Easing.InOutSine
			to: 0
		}
	}
	Behavior on width {
		NumberAnimation {
			easing.type: Easing.InOutSine
			duration: 64
		}
	}
	width: implicitWidth
	Row {
		spacing: 8
		Label {
			id: snackbarLabel
			anchors.verticalCenter: parent.verticalCenter
		}
		Button {
			text: "OK"
			anchors.verticalCenter: parent.verticalCenter
			flat: true
			highlighted: true
			onClicked: snackbarRoot.close();
		}
	}
}
