import QtQuick
import QtQuick.Controls
//import QtMultimedia

import Backend

RootOverlay {
	id: inlineViewRoot
	property MediaSource currentMediaSource: MediaSource {}
	function openMedium(mediaSource) {
		visible = true;
		currentMediaSource = mediaSource;
		mediaItem.forceActiveFocus();
	}
	visible: false
	WaveLoader {
		id: mediaItem
		anchors.fill: parent
		open: inlineViewRoot.visible
		waveChildren: Item {
			anchors.fill: parent
			focus: true
			MediasourceView {
				currentMediaSource: inlineViewRoot.currentMediaSource
				anchors.fill: parent
			}
			Keys.onPressed: (event)=> {
				if (event.key === Qt.Key_Escape) {
					inlineViewRoot.visible = false;
				} else if (event.key === Qt.Key_Left || event.key === Qt.Key_H || event.key === Qt.Key_K) {
					mainTabPage.currentLinks.grid.openLeft();
				} else if (event.key === Qt.Key_Right || event.key === Qt.Key_L || event.key === Qt.Key_J) {
					mainTabPage.currentLinks.grid.openRight();
				}
			}
			ToolButton {
				anchors {top: parent.top; right: parent.right}
				icon.source: "qrc:/navigation/close"
				onClicked: inlineViewRoot.close();
			}
		}
	}
}
