import QtQuick
import QtQuick.Controls
import QtMultimedia
import QtQuick.Layouts

import Backend
Video {
	id: video
	required property MediaSource currentMediaSource
	anchors.fill: parent
	playbackRate: playbackSpeed.currentValue
	visible: currentMediaSource.type === Media.VIDEO && currentMediaSource.location !== ""
	source: visible ? "file://" + currentMediaSource.location : ""
	onErrorStringChanged: c_gui.snackbarText = errorString;
	onSourceChanged: {
		if (source !== "") {
			video.play();
		}
	}
	MouseArea {
		id: mouseArea
		anchors.fill: parent
		anchors.topMargin: parent.height - scrubLayout.implicitHeight * 2
		hoverEnabled: true
	}
	RowLayout {
		anchors { bottom: parent.bottom; left: parent.left; right: parent.right; bottomMargin: mouseArea.containsMouse ? 0 : -height }
		id: scrubLayout
		visible: bottomMargin !== -height
		Behavior on anchors.bottomMargin {
			NumberAnimation {
				duration: 150
				easing.type: Easing.InOutSine
			}
		}
		TimeLabel {
			time: video.position
		}
		Slider {
			Layout.fillWidth: true
			focusPolicy: Qt.NoFocus
			to: video.duration
			value: video.position
			onMoved: video.seek(position * to);
		}
		TimeLabel {
			time: video.duration
		}
		ComboBox {
			id: playbackSpeed
			model: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]
			currentIndex: 2
			displayText: currentText + "x"
			implicitWidth: 64
		}
		ToolButton {
			icon.source: "qrc:/image/rotate_90_degrees_ccw"
			onClicked: video.orientation += 90
		}
	}
}
