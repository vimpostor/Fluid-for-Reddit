import QtQuick
import QtQuick.Controls

Label {
	property int time: 0
	property int minutes: Math.floor(time / 60000)
	property int seconds: Math.floor(time / 1000) % 60
	text: minutes + ":" + ((seconds < 10) ? "0" : "") + seconds
}
