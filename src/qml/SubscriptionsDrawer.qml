import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import Backend

AutoDrawer {
	ListView {
		id: subscriptionsListView
		property string searchFilter
		anchors.fill: parent
		model: c_subscriptionsModel
		header: Pane {
			z: 2
			width: parent.width
			contentHeight: headerColumnLayout.implicitHeight
			ColumnLayout {
				id: headerColumnLayout
				anchors.left: parent.left
				anchors.right: parent.right
				TextField {
					id: subredditSearch
					placeholderText: "Goto subreddit"
					Layout.fillWidth: true
					onAccepted: c_tabmodel.getData(mainTabPage.currentIndex).requestSubreddit(text, 0);
					Binding {
						target: subscriptionsListView
						property: "searchFilter"
						value: subredditSearch.text
					}
				}
				RowLayout {
					Layout.fillWidth: true
					Label {
						id: subredditLabel
						text: "Subscriptions"
						Layout.fillWidth: true
					}
					ToolButton {
						icon.source: "qrc:/navigation/refresh"
						enabled: c_backend.isLoggedIn && !c_subscriptionsModel.refreshing
						onClicked: c_subscriptionsModel.refresh();
					}
				}
			}
		}
		headerPositioning: ListView.PullBackHeader
		delegate: ItemDelegate {
			width: parent ? parent.width : 0
			visible: subreddit.display_name.includes(subscriptionsListView.searchFilter)
			text: subreddit.display_name
			onClicked: c_tabmodel.getData(mainTabPage.currentIndex).requestSubreddit(text, Collection.HOT);
		}
	}
}
