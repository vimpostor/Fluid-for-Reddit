import QtQuick
import QtQuick.Controls

Label {
	wrapMode: Text.WordWrap
	textFormat: Text.MarkdownText
	onLinkActivated: c_mediaParser.openUrl(link);
}
