#pragma once

#include <QTemporaryFile>
#include <QtNetwork>
#include <memory>
#include <queue>

#include "Util/util.hpp"
#include "Api/Media/mediasource.hpp"
#include "gui.hpp"

class Downloader : public QObject
{
	Q_OBJECT
public:
	explicit Downloader(QObject* parent = nullptr);

	SINGLETON(Downloader)
	void queueDownload(const std::shared_ptr<MediaSource>& mediaSource);
	void download(const std::shared_ptr<MediaSource>& mediaSource);
private:
	void downloadReady(QNetworkReply* reply, const std::shared_ptr<MediaSource>& mediaSource);
	void tryNextDownload();

	std::queue<std::shared_ptr<MediaSource>> downloadQueue;
	static constexpr int kMaxConcurrentDownloads = 4;
	QSemaphore downloadSemaphore {kMaxConcurrentDownloads};
};
