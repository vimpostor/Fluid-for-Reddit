#include "cache.hpp"

#include <utility>

void Cache::init()
{
	cache = std::make_unique<QSettings>(QCoreApplication::organizationName(), ".cache");
}

Listing<Subreddit> Cache::getSubscriptions() const
{
	Listing<Subreddit> result;
	int size = cache->beginReadArray("subscriptions");
	for (int i = 0; i < size; ++i) {
		cache->setArrayIndex(i);
		Subreddit sub;
		sub.restore(cache.get());
		result.emplace_back(std::move(sub));
	}
	cache->endArray();
	return result;
}

void Cache::setSubscriptions(Listing<Subreddit>& sInfo)
{
	cache->beginWriteArray("subscriptions", sInfo.size());
	for (int i = 0; i < sInfo.size(); ++i) {
		cache->setArrayIndex(i);
		sInfo[i].store(cache.get());
	}
	cache->endArray();
}

Token Cache::getToken(QString name) const
{
	Token t(name);
	t << *cache;
	return t;
}

void Cache::setToken(const Token& t)
{
	*cache << t;
}
