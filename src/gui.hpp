#pragma once

#include <QObject>
#include <QDesktopServices>
#include <QDebug>

#include "settings.hpp"
#include "Api/Media/mediasource.hpp"

class Gui : public QObject
{
	Q_OBJECT

	QML_INIT_WRITABLE_VAR_PROPERTY(QString, windowTitle, "Fluid for Reddit")
	QML_WRITABLE_VAR_PROPERTY(QString, snackbarText)
public:
	SINGLETON(Gui)

	Q_INVOKABLE void openUrlDontAsk(const QUrl& url);
	Q_INVOKABLE void copyToClipboard(const QString& text);
	void error(const QString& error);
	void openMedium(const std::shared_ptr<MediaSource>& mediaSource);
signals:
	void confirmOpenUrl(const QUrl &url);

	void openImage(MediaSource* mediaSource);
	void openAlbum(MediaSource* mediaSource);
	void openVideo(MediaSource* mediaSource);
public slots:
	void openUrl(const QUrl &url);
};
