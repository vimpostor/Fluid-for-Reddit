#include "token.hpp"

#include <utility>

constexpr const int EXPIRES_SOON_OFFSET = 120;


Token::Token()
{
	this->expiration = QDateTime::currentDateTime();
}

Token::Token(QString name, QString token) : Token()
{
	this->name = name;
	this->token = token;
}

bool Token::isValid() const
{
	return expiration > QDateTime::currentDateTime();
}

bool Token::expiresSoon() const
{
	return expiration < QDateTime::currentDateTime().addSecs(EXPIRES_SOON_OFFSET);
}

QDateTime Token::recommendedRefreshal() const
{
	return expiration.addSecs(-EXPIRES_SOON_OFFSET);
}

int Token::msecsToRecommendedRefreshal() const
{
	return QDateTime::currentDateTime().msecsTo(recommendedRefreshal());
}

QSettings &operator <<(QSettings &settings, const Token& t)
{
	settings.beginGroup(t.name);
	settings.setValue("token", t.token);
	settings.setValue("expiration", t.expiration);
	settings.endGroup();
	return settings;
}

Token &operator <<(Token &t, QSettings &settings)
{
	settings.beginGroup(t.name);
	t.token = settings.value("token").toString();
	t.expiration = settings.value("expiration").toDateTime();
	settings.endGroup();
	return t;
}
