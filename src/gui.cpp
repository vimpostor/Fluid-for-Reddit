#include "gui.hpp"

#include <QGuiApplication>
#include <QClipboard>

void Gui::openUrlDontAsk(const QUrl &url)
{
	QDesktopServices::openUrl(url);
}

void Gui::copyToClipboard(const QString& text)
{
	QGuiApplication::clipboard()->setText(text);
}

void Gui::error(const QString& error)
{
	qDebug() << error;
	set_snackbarText(error);
}

void Gui::openMedium(const std::shared_ptr<MediaSource>& mediaSource)
{
	switch (mediaSource->get_type()) {
	case Media::Type::LINK: {
		openUrl(mediaSource->get_link());
		break;
	}
	case Media::Type::IMAGE: {
		emit openImage(mediaSource.get());
		break;
	}
	case Media::Type::ALBUM: {
		emit openAlbum(mediaSource.get());
		break;
	}
	case Media::Type::VIDEO: {
		emit openVideo(mediaSource.get());
		break;
	}
	default: {
		set_snackbarText("Cannot open Medium with no media type");
	}
	}
}

void Gui::openUrl(const QUrl &url)
{
	// TODO: Handle Reddit Links in the app itself
	if (Settings::get()->getConfirmLinks()) {
		emit confirmOpenUrl(url);
	} else {
		openUrlDontAsk(url);
	}
}
