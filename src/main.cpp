#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQmlContext>
#include <QIcon>

#include "Models/linkmodel.hpp"
#include "Models/commentmodel.hpp"
#include "Models/subscriptionsmodel.hpp"
#include "Models/tabmodel.hpp"
#include "downloader.hpp"
#include "backend.hpp"
#include "settings.hpp"
#include "cache.hpp"
#include "Api/api.hpp"

#ifndef VERSION
#define VERSION "0.1"
#endif

int main(int argc, char* argv[])
{
	QCoreApplication::setOrganizationName("Fluid for Reddit");
	QCoreApplication::setApplicationName("Fluid for Reddit");
	QCoreApplication::setApplicationVersion(VERSION);
	QQuickStyle::setStyle(QLatin1String("Material"));
#if defined(OS_DESKTOP)
	constexpr const char* materialVariantName = "QT_QUICK_CONTROLS_MATERIAL_VARIANT";
	if (!qEnvironmentVariableIsSet(materialVariantName)) {
		// dense Material style, more suited for desktop targets
		qputenv(materialVariantName, "Dense");
	}
#endif
	QGuiApplication app(argc, argv);

	// set application icon
	QGuiApplication::setWindowIcon(QIcon::fromTheme("fluid-for-reddit", QIcon(":/fluid-for-reddit")));

	Util::getNetworkAccessManager()->setAutoDeleteReplies(true);
	Settings::get()->init();
	Cache::get()->init();

	// register QML types here
	qmlRegisterType<LinkModel>("Backend", 1, 0, "LinkModel");
	qmlRegisterType<CommentModel>("Backend", 1, 0, "CommentModel");
	qmlRegisterType<SubscriptionsModel>("Backend", 1, 0, "SubscriptionsModel");
	qmlRegisterType<AlbumModel>("Backend", 1, 0, "AlbumModel");
	qmlRegisterType<TabModel>("Backend", 1, 0, "TabModel");
	qmlRegisterType<Downloader>("Backend", 1, 0, "Downloader");
	qmlRegisterType<Media>("Backend", 1, 0, "Media");
	qmlRegisterType<Vote>("Backend", 1, 0, "Vote");
	qmlRegisterType<Collection>("Backend", 1, 0, "Collection");
	qmlRegisterType<MediaParser>("Backend", 1, 0, "MediaParser");
	qmlRegisterType<Backend>("Backend", 1, 0, "Backend");
	qmlRegisterType<Settings>("Backend", 1, 0, "Settings");
	qmlRegisterType<VotableView>("Backend", 1, 0, "VotableView");
	qmlRegisterType<SubredditWrapper>("Backend", 1, 0, "SubredditWrapper");
	qmlRegisterType<MediaSource>("Backend", 1, 0, "MediaSource");
	qmlRegisterType<SubredditView>("Backend", 1, 0, "SubredditView");
	qmlRegisterType<LinkView>("Backend", 1, 0, "LinkView");

	QQmlApplicationEngine engine;
	engine.addImportPath(QStringLiteral(":/"));

	// register QML context properties here
	engine.rootContext()->setContextProperty("c_backend", Backend::get());
	engine.rootContext()->setContextProperty("c_settings", Settings::get());
	engine.rootContext()->setContextProperty("c_api", Api::get());
	engine.rootContext()->setContextProperty("c_subscriptionsModel", SubscriptionsModel::get());
	engine.rootContext()->setContextProperty("c_tabmodel", TabModel::get());
	engine.rootContext()->setContextProperty("c_gui", Gui::get());
	engine.rootContext()->setContextProperty("c_mediaParser", MediaParser::get());

	engine.load(QUrl(QStringLiteral("qrc:/Backend/src/qml/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	OAuth::get()->initialize();
	return QGuiApplication::exec();
}
