#pragma once

#include <QObject>

#include "Api/Types/link.hpp"
#include "Views/votableview.hpp"

class LinkView : public QObject
{
	Q_OBJECT
	QML_WRITABLE_PTR_PROPERTY(Link, object)
	QML_OBJECT_PROPERTY(VotableView, votable)
	QML_VIEW_PTR_PROPERTY(MediaSource, mediaSource)

public:
	QML_VIEW_PROPERTY(QString, title)
	QML_VIEW_PROPERTY(int, num_comments)
	QML_VIEW_PROPERTY(bool, thumbnailReady)
	QML_VIEW_PTR_PROPERTY(MediaSource, thumbnailLocal)
public:
	explicit LinkView(Link* object = nullptr, QObject* parent = nullptr);
};

