#pragma once

#include <QObject>

#include "Api/Types/votable.hpp"

class VotableView : public QObject
{
	Q_OBJECT
	QML_WRITABLE_PTR_PROPERTY(Votable, object)

	QML_VIEW_PROPERTY(int, ups)
	QML_VIEW_PROPERTY(Vote::Direction, likes)
public:
	explicit VotableView(Votable* object = nullptr, QObject *parent = nullptr);
};

