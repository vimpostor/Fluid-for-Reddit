#pragma once

#include <QObject>

#include "Api/Types/subreddit.hpp"

class SubredditView : public QObject
{
	Q_OBJECT
	QML_WRITABLE_PTR_PROPERTY(Subreddit, object)

	Q_PROPERTY(QString display_name READ (m_object->get_display_name) CONSTANT)
public:
	explicit SubredditView(Subreddit* object = nullptr, QObject *parent = nullptr);
};

