#pragma once

#include <variant>

#include "Util/listmodelhelper.hpp"
#include "linkmodel.hpp"
#include "commentmodel.hpp"

class TabModel : public QAbstractListModel
{
	Q_OBJECT
public:
	TabModel();

	SINGLETON(TabModel)

	Q_INVOKABLE QVariant getData(const int i) const;
	virtual int rowCount(const QModelIndex&) const override;
	virtual QVariant data(const QModelIndex& index, int role) const override;
	virtual QHash<int, QByteArray> roleNames() const override;
	Q_INVOKABLE void append();
	Q_INVOKABLE void remove(int index);
	void appendLinkModel(std::shared_ptr<LinkModel> linkModel);
	void appendCommentsModel(std::shared_ptr<CommentModel> commentModel);
	void appendMediasource(std::shared_ptr<MediaSource>& mediaSource);
private:
	std::vector<std::variant<std::shared_ptr<LinkModel>, std::shared_ptr<CommentModel>, std::shared_ptr<MediaSource>>> m_data;
};
