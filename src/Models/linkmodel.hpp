#pragma once

#include <QAbstractListModel>
#include <vector>

#include "Api/api.hpp"
#include "Api/Media/mediaparser.hpp"
#include "Api/Wrappers/subredditwrapper.hpp"
#include "Views/linkview.hpp"

class LinkModel : public QAbstractListModel
{
	Q_OBJECT

	QML_INIT_WRITABLE_VAR_PROPERTY(bool, loading, false)
	QML_SHARED_PROPERTY(SubredditWrapper, subredditWrapper)
public:
	LinkModel();

	virtual int rowCount(const QModelIndex&) const override;
	virtual QVariant data(const QModelIndex& index, int role) const override;
	virtual QHash<int, QByteArray> roleNames() const override;

	Q_INVOKABLE void requestSubreddit(const QString& subreddit, const int sort);
	Q_INVOKABLE void requestMoreResults();
	Q_INVOKABLE void requestSubredditInfo();
	Q_INVOKABLE void preloadMedium(int currentIndex, bool invertDirection = false);
private slots:
	void linksUpdated(Listing<std::shared_ptr<Link>> newLinks);
private:
	void refreshViews();
	void requestLinks();

	enum RoleNames {
		LinkRole = Qt::UserRole,
	};
	Listing<std::unique_ptr<LinkView>> m_views;
	QHash<int, QByteArray> m_roleNames {{LinkRole, "link"}};
};
