#include "albummodel.hpp"

int AlbumModel::rowCount(const QModelIndex&) const
{
	return m_data.size();
}

QVariant AlbumModel::data(const QModelIndex& index, int /*role*/) const
{
	return QVariant::fromValue(m_data[index.row()].get());
}

QHash<int, QByteArray> AlbumModel::roleNames() const
{
	return m_roleNames;
}

void AlbumModel::appendChild(const std::shared_ptr<MediaSource>& child)
{
	emit beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
	m_data.push_back(child);
	emit endInsertRows();
}
