#pragma once

#include <QAbstractListModel>

#include "Api/api.hpp"
#include "cache.hpp"
#include "Views/subredditview.hpp"

class SubscriptionsModel : public QAbstractListModel
{
	Q_OBJECT

	QML_INIT_READONLY_VAR_PROPERTY(bool, refreshing, true)
public:
	SubscriptionsModel();
	SINGLETON(SubscriptionsModel)

	virtual int rowCount(const QModelIndex& ) const override;
	virtual QVariant data(const QModelIndex& index, int role) const override;
	virtual QHash<int, QByteArray> roleNames() const override;

	Q_INVOKABLE void refresh();
private:
	void subscriptionsReady(Listing<Subreddit> &s);
	void refreshViews();

	Listing<Subreddit> m_data;
	Listing<std::unique_ptr<SubredditView>> m_views;
	QHash<int, QByteArray> m_roleNames = {{SubredditRole, "subreddit"}};
	enum RoleNames {
		SubredditRole = Qt::UserRole,
	};
};
