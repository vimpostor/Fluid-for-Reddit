#include "linkmodel.hpp"

LinkModel::LinkModel()
{
	m_subredditWrapper = std::make_shared<SubredditWrapper>();
	connect(m_subredditWrapper.get(), &SubredditWrapper::updated, this, &LinkModel::linksUpdated);
}

int LinkModel::rowCount(const QModelIndex&) const
{
	return static_cast<int>(m_subredditWrapper->links.size());
}

QVariant LinkModel::data(const QModelIndex& index, int) const
{
	auto& l = m_views[static_cast<uint>(index.row())];
	return QVariant::fromValue(l.get());
}

QHash<int, QByteArray> LinkModel::roleNames() const
{
	return m_roleNames;
}

void LinkModel::requestSubreddit(const QString& subreddit, const int sort)
{
	if (subreddit.isEmpty()) {
		return;
	}
	emit beginResetModel();
	m_subredditWrapper->links.clear();
	emit endResetModel();
	m_subredditWrapper->get_subreddit()->set_display_name(subreddit);
	m_subredditWrapper->sorting = static_cast<Collection::Sorting>(sort);
	m_subredditWrapper->links.after.clear();
	requestLinks();
}

void LinkModel::requestMoreResults()
{
	// exit if already reloading
	if (get_loading()) {
		return;
	}
	requestLinks();
}

void LinkModel::requestSubredditInfo()
{
	Api::get()->getSubreddit(m_subredditWrapper->get_subreddit());
}

/**
 * @brief Preloads media from neighbor elements
 * @param currentIndex The index of the current element
 * @param invertDirection Set this to true, if you want to preload the left neighbor.
 */
void LinkModel::preloadMedium(int currentIndex, bool invertDirection)
{
	const auto neighborIndex = currentIndex + 1 - 2 * invertDirection;
	if (neighborIndex >= 0 && neighborIndex < rowCount(QModelIndex())) {
		const auto neighbor = m_subredditWrapper->links[neighborIndex];
		MediaParser::get()->parseLink(neighbor.get());
	}
}

void LinkModel::linksUpdated(Listing<std::shared_ptr<Link> > newLinks)
{
	emit beginInsertRows(QModelIndex(), static_cast<int>(m_subredditWrapper->links.size()), static_cast<int>(m_subredditWrapper->links.size() + newLinks.size()) - 1);
	m_subredditWrapper->links.appendListing(newLinks);
	refreshViews();
	emit endInsertRows();
	set_loading(false);
}

void LinkModel::refreshViews()
{
	m_views.clear();
	for (auto& s : m_subredditWrapper->links) {
		m_views.emplace_back(std::make_unique<LinkView>(s.get()));
	}
}

void LinkModel::requestLinks()
{
	set_loading(true);
	auto* result = Api::get()->getSubredditLinks(m_subredditWrapper.get());
	connect(result, &ApiResultSignal::ready, [=, this](){ auto res = result->get(); emit m_subredditWrapper->updated(res); });
}
