#pragma once

#include <QAbstractListModel>

#include "Api/Wrappers/commentswrapper.hpp"

class CommentModel : public QAbstractListModel
{
	Q_OBJECT

	QML_OBJECT_PROPERTY(CommentsWrapper, commentsWrapper)
public:
	CommentModel();

	virtual int rowCount(const QModelIndex&) const override;
	virtual QVariant data(const QModelIndex& index, int role) const override;
	virtual QHash<int, QByteArray> roleNames() const override;
private slots:
	void commentsUpdated(Node<std::shared_ptr<Comment>> newComments);
private:
	enum RoleNames {
		CommentRole = Qt::UserRole,
	};
	QHash<int, QByteArray> m_roleNames {{CommentRole, "comment"}};
};
