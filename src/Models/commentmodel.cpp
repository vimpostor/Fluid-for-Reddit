#include "commentmodel.hpp"

CommentModel::CommentModel()
{
	connect(&m_commentsWrapper, &CommentsWrapper::updated, this, &CommentModel::commentsUpdated);
}

int CommentModel::rowCount(const QModelIndex& ) const
{
	return static_cast<int>(m_commentsWrapper.comments.childrenSize(true));
}

QVariant CommentModel::data(const QModelIndex& index, int role) const
{
	auto& c = m_commentsWrapper.comments.getFlattenedChildByIndex(index.row(), true).data;
	switch (role) {
	default:
//		return QVariant::fromValue(c.get());
		return QVariant();
	}
}

QHash<int, QByteArray> CommentModel::roleNames() const
{
	return m_roleNames;
}

void CommentModel::commentsUpdated(Node<std::shared_ptr<Comment> > newComments)
{
	emit beginInsertRows(QModelIndex(), static_cast<int>(0), static_cast<int>(newComments.childrenSize(true)) - 1);
	// TODO: Actually append the comments
	m_commentsWrapper.comments = newComments;
	emit endInsertRows();
}
