#pragma once

#include <QAbstractListModel>
#include <memory>

#include "Api/Media/mediasource.hpp"

class AlbumModel : public QAbstractListModel
{
	Q_OBJECT
public:
	virtual int rowCount(const QModelIndex&) const override;
	virtual QVariant data(const QModelIndex& index, int role) const override;
	virtual QHash<int, QByteArray> roleNames() const override;

	void appendChild(const std::shared_ptr<MediaSource>& child);
private:
	std::vector<std::shared_ptr<MediaSource>> m_data;
	enum RoleNames {
		MediaSourceRole = Qt::UserRole,
	};
	QHash<int, QByteArray> m_roleNames = {{MediaSourceRole, "mediaSource"}};
};
