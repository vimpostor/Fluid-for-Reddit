#include "tabmodel.hpp"

TabModel::TabModel()
{
	append();
}

QVariant TabModel::getData(const int i) const
{
	const auto d = m_data[i];
	QVariant result;
	if (std::holds_alternative<std::shared_ptr<LinkModel>>(d)) {
		const auto castResult = std::get<std::shared_ptr<LinkModel>>(d).get();
		QQmlEngine::setObjectOwnership(castResult, QQmlEngine::CppOwnership);
		result = QVariant::fromValue(castResult);
	} else if (std::holds_alternative<std::shared_ptr<CommentModel>>(d)) {
		const auto castResult = std::get<std::shared_ptr<CommentModel>>(d).get();
		QQmlEngine::setObjectOwnership(castResult, QQmlEngine::CppOwnership);
		result = QVariant::fromValue(castResult);
	} else if (std::holds_alternative<std::shared_ptr<MediaSource>>(d)) {
		const auto castResult = std::get<std::shared_ptr<MediaSource>>(d).get();
		QQmlEngine::setObjectOwnership(castResult, QQmlEngine::CppOwnership);
		result = QVariant::fromValue(castResult);
	}
	return result;
}

int TabModel::rowCount(const QModelIndex&) const
{
	return m_data.size();
}

QVariant TabModel::data(const QModelIndex& index, int role) const
{
	const auto i = index.row();
	if (role == Qt::UserRole) {
		return getData(i);
	} else {
		const auto d = m_data[i];
		if (std::holds_alternative<std::shared_ptr<LinkModel>>(d)) {
			return "LinkmodelDelegate.qml";
		} else if (std::holds_alternative<std::shared_ptr<CommentModel>>(d)) {
			return "CommentmodelDelegate.qml";
		} else if (std::holds_alternative<std::shared_ptr<MediaSource>>(d)) {
			return "MediasourceDelegate.qml";
		} else {
			return "";
		}
	}
}

QHash<int, QByteArray> TabModel::roleNames() const
{
	return {{Qt::UserRole, "delegate_model"}, {Qt::UserRole + 1, "delegate_item"}};
}

void TabModel::append()
{
	const auto size = m_data.size();
	emit beginInsertRows(QModelIndex(), size, size);
	m_data.emplace_back(std::make_shared<LinkModel>());
	emit endInsertRows();
}

void TabModel::remove(int index)
{
	emit beginRemoveRows(QModelIndex(), index, index);
	m_data.erase(m_data.begin() + index);
	emit endRemoveRows();
}

void TabModel::appendLinkModel(std::shared_ptr<LinkModel> linkModel)
{
	const auto size = m_data.size();
	emit beginInsertRows(QModelIndex(), size, size);
	m_data.emplace_back(linkModel);
	emit endInsertRows();
}

void TabModel::appendCommentsModel(std::shared_ptr<CommentModel> commentModel)
{
	const auto size = m_data.size();
	emit beginInsertRows(QModelIndex(), size, size);
	m_data.emplace_back(commentModel);
	emit endInsertRows();
}

void TabModel::appendMediasource(std::shared_ptr<MediaSource>& mediaSource)
{
	const auto size = m_data.size();
	emit beginInsertRows(QModelIndex(), size, size);
	m_data.emplace_back(mediaSource);
	emit endInsertRows();
}
