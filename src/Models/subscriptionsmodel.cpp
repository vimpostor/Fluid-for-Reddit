#include "subscriptionsmodel.hpp"

SubscriptionsModel::SubscriptionsModel()
{
	m_data = Cache::get()->getSubscriptions();
	refreshViews();
	this->set_refreshing(false);
}

int SubscriptionsModel::rowCount(const QModelIndex &) const
{
	return m_views.size();
}

QVariant SubscriptionsModel::data(const QModelIndex &index, int) const
{
	auto& s = m_views[static_cast<uint>(index.row())];
	return QVariant::fromValue(s.get());
}

QHash<int, QByteArray> SubscriptionsModel::roleNames() const
{
	return m_roleNames;
}

void SubscriptionsModel::refresh()
{
	this->set_refreshing(true);
	auto* result = Api::get()->getSubscriptions();
	connect(result, &ApiResultSignal::ready, [=, this](){ auto res = result->get(); subscriptionsReady(res); });
}

void SubscriptionsModel::subscriptionsReady(Listing<Subreddit> &s)
{
	beginResetModel();
	// sort the model first
	std::ranges::sort(s, [](auto a, auto b){
		return a < b;
	});
	m_data = std::move(s);
	refreshViews();
	endResetModel();
	Cache::get()->setSubscriptions(m_data);
	this->set_refreshing(false);
}

void SubscriptionsModel::refreshViews()
{
	m_views.clear();
	for (auto& s : m_data) {
		m_views.emplace_back(std::make_unique<SubredditView>(&s));
	}
}
