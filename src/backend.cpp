#include "backend.hpp"

Backend::Backend(QObject* parent) : QObject(parent)
{
	updateIsLoggedIn(Settings::get()->getUserStatus());
	connect(Settings::get(), &Settings::userStatusChanged, this, &Backend::updateIsLoggedIn);
}

void Backend::setUserStatus(const int status)
{
	const auto userStatus = static_cast<Settings::UserStatus>(status);
	Settings::get()->setUserStatus(userStatus);
	OAuth::get()->grant();
}

void Backend::openUrl(const QUrl &url)
{
	Gui::get()->openUrl(url);
}

void Backend::openUrlDontAsk(const QUrl &url)
{
	Gui::get()->openUrlDontAsk(url);
}

void Backend::openComments(Link* link)
{
	auto commentModel = std::make_shared<CommentModel>();
//	commentModel->set_commentsWrapper(link->get_comments());
	TabModel::get()->appendCommentsModel(commentModel);
	Api::get()->getComments(link);
}

void Backend::openNewMediasourceTab(Link* link)
{
	MediaParser::get()->parseLink(link);
	TabModel::get()->appendMediasource(link->get_mediaSource_shared());
}

void Backend::updateIsLoggedIn(const Settings::UserStatus s)
{
	set_isLoggedIn(s == Settings::UserStatus::SignedIn);
}
