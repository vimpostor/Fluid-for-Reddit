#include "settings.hpp"

constexpr const char* REFRESH_TOKEN = "refreshtoken";
constexpr const char* USER_STATUS = "userstatus";
constexpr const char* OPEN_LINKS_EXTERNALLY = "open_links_externally";
constexpr const char* CONFIRM_LINKS = "confirm_links";
constexpr const char* MEDIA_CACHE_LIMIT = "media_cache_limit";

void Settings::init()
{
	// uses organisation name and application name, which is set in main.cpp
	settings = std::make_unique<QSettings>();
}

QString Settings::getRefreshToken() const
{
	return settings->value(REFRESH_TOKEN).toString();
}

void Settings::setRefreshToken(const QString& token)
{
	settings->setValue(REFRESH_TOKEN, token);
}

Settings::UserStatus Settings::getUserStatus() const
{
	return static_cast<UserStatus>(settings->value(USER_STATUS, UserStatus::SigningIn).toInt());
}

void Settings::setUserStatus(const Settings::UserStatus status)
{
	settings->setValue(USER_STATUS, status);
	emit userStatusChanged(status);
}

bool Settings::getOpenLinksExternally() const
{
	return settings->value(OPEN_LINKS_EXTERNALLY, false).toBool();
}

void Settings::setOpenLinksExternally(bool s)
{
	settings->setValue(OPEN_LINKS_EXTERNALLY, s);
}

bool Settings::getConfirmLinks() const
{
	return settings->value(CONFIRM_LINKS, true).toBool();
}

void Settings::setConfirmLinks(bool s)
{
	settings->setValue(CONFIRM_LINKS, s);
}

uint Settings::getMediaCacheLimit() const
{
	constexpr uint defaultLimit = 2_GB;
	return settings->value(MEDIA_CACHE_LIMIT, defaultLimit).toUInt();
}

void Settings::setMediaCacheLimit(const uint m)
{
	settings->setValue(MEDIA_CACHE_LIMIT, m);
	emit mediaCacheLimitChanged(m);
}
