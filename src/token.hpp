#pragma once

#include <QDateTime>
#include <QSettings>

class Token
{
public:
	explicit Token();
	Token(QString name, QString token = "");
	bool isValid() const;
	bool expiresSoon() const;
	QDateTime recommendedRefreshal() const;
	int msecsToRecommendedRefreshal() const;

	QString name;
	QString token;
	QDateTime expiration;
};

QSettings& operator <<(QSettings& settings, const Token& t);

Token& operator <<(Token& t, QSettings& settings);
