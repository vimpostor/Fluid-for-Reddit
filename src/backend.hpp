#pragma once

#include <QObject>

#include "Api/api.hpp"
#include "Models/tabmodel.hpp"

class Backend : public QObject
{
	Q_OBJECT

	QML_INIT_WRITABLE_VAR_PROPERTY(bool, isLoggedIn, false);
public:
	explicit Backend(QObject* parent = nullptr);
	SINGLETON(Backend)

	Q_INVOKABLE void setUserStatus(const int status);
	Q_INVOKABLE void openUrl(const QUrl &url);
	Q_INVOKABLE void openUrlDontAsk(const QUrl &url);
	Q_INVOKABLE void openComments(Link* link);
	Q_INVOKABLE void openNewMediasourceTab(Link* link);
signals:
private slots:
private:
	void updateIsLoggedIn(const Settings::UserStatus s);
};
